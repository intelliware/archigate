import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import Profiles from '../imports/collections/Profiles';
import Photos from '../imports/collections/Photos';
import {Router} from 'react-router';
import {promiseFind} from '../lib/utils';
import App from '../imports/ui/pages/App.jsx';
import { renderRoutes } from './routes';

Meteor.startup(() => {
  render(renderRoutes(), document.getElementById('render-target'));

});