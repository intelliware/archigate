/**
 * Created by mehdi on 6/10/16.
 */
import React from 'react';
import { IndexRoute, Router, Route, browserHistory } from 'react-router';

// route components
import App from '../imports/ui/pages/App.jsx';
import HomeContainer from '../imports/ui/pages/Home.jsx';
import NotFound404 from '../imports/ui/pages/NotFound404.jsx';
import Login from '../imports/ui/components/accounts/Login/Login.jsx';
import Accounts from '../imports/ui/pages/Accounts/Accounts.jsx';
import SignUp from '../imports/ui/components/accounts/SignUp/SignUp.jsx';
import ProfilePage from '../imports/ui/pages/Profile/Profile.jsx';
import ImageSearch from '../imports/ui/components/Search/ImageSearch.jsx';
import ProsSearch from '../imports/ui/components/Search/ProsSearch.jsx';
import DiscussionsSearch from '../imports/ui/components/Search/DiscussionsSearch.jsx';
import StoriesSearch from '../imports/ui/components/Search/StoriesSearch.jsx';
import Search from '../imports/ui/pages/Search/Search.jsx';
import Photo from '../imports/ui/pages/Photo/Photo.jsx';
import ProfileIdeaBoxSection from '../imports/ui/components/profile/ProfileIdeaBoxSection.jsx';
import ProfileTeamsSection from '../imports/ui/components/profile/ProfileTeamsSection.jsx';
import ProfileOthersReviewsSection from '../imports/ui/components/profile/ProfileOthersReviewsSection.jsx';
import ProfileGapboxSection from '../imports/ui/components/profile/ProfileGapboxSection.jsx';
import ProfileOthersReviews from '../imports/ui/components/profile/ProfileOthersReviewsSection.jsx';
import ProjectOverviewCard from '../imports/ui/components/profile/ProfileProjectSection.jsx';
import ProfileIdeaBoxOverviewCard from '../imports/ui/components/profile/ProfileIdeaBoxSection.jsx';
import Register from '../imports/ui/pages/Register/Register.jsx'
import ChooseAccountType from '../imports/ui/components/Register/ChooseAccountType.jsx';
import PersonalUserInfoStep1 from '../imports/ui/components/Register/PersonalUserInfoStep1.jsx';
import ProfessionalUserInfoStep1 from '../imports/ui/components/Register/ProfessionalUserInfoStep1.jsx';
import PersonalUserSpecificInfoStep2 from '../imports/ui/components/Register/PersonalUserSpecificInfoStep2.jsx';
import ProfessionalUserSpecificInfoStep2 from '../imports/ui/components/Register/ProfessionalUserSpecificInfoStep2.jsx';
import FinalStep from '../imports/ui/components/Register/FinalStep.jsx'; 
//helpers
import { requireAuth } from '../imports/utility/helpers.js'

export const renderRoutes = () => (
    <Router history={browserHistory}>
        <Route path="/" component={App}>
            <IndexRoute component={HomeContainer}/>
            <Route path="accounts" component={Accounts}>
                <IndexRoute component={Login}/>
                <Route path="signup" component={SignUp}/>
            </Route>
            <Route path="register" component={Register}>
                <IndexRoute component={ChooseAccountType} />
                <Route path="personal/step1" component={PersonalUserInfoStep1} />
                <Route path="professional/step1" component={ProfessionalUserInfoStep1} />
                <Route path="professional/step2" component={ProfessionalUserSpecificInfoStep2} />
                <Route path="personal/step2" component={PersonalUserSpecificInfoStep2} />
                <Route path="finalStep" component={FinalStep} />
            </Route>
            <Route path="profile/:id" component={ProfilePage} onEnter={requireAuth}>
                <IndexRoute component={ProjectOverviewCard} />
                <Route path="ideaBox" component={ProfileIdeaBoxSection} />
                <Route path="merchandise" component={ProsSearch} />
                <Route path="otherReviews" component={ProfileOthersReviewsSection} />
                <Route path="gapBox" component={ProfileGapboxSection} />
                <Route path="contacts" component={ProfileTeamsSection} />
                <Route path="contacts" component={ProsSearch} />
                <Route path="followUp" component={ProsSearch} />
                <Route path="message" component={ProsSearch} />
            </Route>
            <Route path="search" component={Search}>
                <IndexRoute component={ImageSearch} />
                <Route path="professionals" component={ProsSearch} />
                <Route path="discussions" component={DiscussionsSearch} />
                <Route path="stories" component={StoriesSearch} />
            </Route>
            <Route path="photos/:id" component={Photo} />
            <Route path="*" component={NotFound404}/>
        </Route>
    </Router>
);