/**
 * Created by mehdi on 7/6/16.
 */
import Comments from '../imports/collections/Comments';
import Projects from '../imports/collections/Projects';
import IdeaBoxes from '../imports/collections/IdeaBoxes';
import Photos from '../imports/collections/Photos';
import Reviews from '../imports/collections/Reviews';
import Stories from '../imports/collections/Stories';
import Categories from '../imports/collections/Categories';
import Teams from '../imports/collections/Teams';
import Profiles from '../imports/collections/Profiles';
import Discussions from '../imports/collections/Discussions';

import {promiseInsert, promiseUpdate, promiseCall} from '../lib/utils';


Meteor.startup(() => {
    let amirhossein = Accounts.findUserByEmail("amirhossein.nouranizadeh@gmail.com");
    let amirId = 0;
    if(amirhossein)
        amirId = amirhossein._id;
    else
        amirId = Accounts.createUser({
            email: 'amirhossein.nouranizadeh@gmail.com',
            password: '1234567890'
        });
    let ramin = Accounts.findUserByEmail("rekino@gmail.com");
    let raminId = 0;
    if(ramin)
        raminId = ramin._id;
    else
        raminId = Accounts.createUser({
            email: 'rekino@gmail.com',
            password: '1234567890'
        });

    let mehdi = Accounts.findUserByEmail("balouchi@outlook.com");
    let mehdiId = 0;
    if(mehdi)
        mehdiId = mehdi._id;
    else
        mehdiId = Accounts.createUser({
            email: 'balouchi@outlook.com',
            password: '1234567890'
        });

    /* Adding sample categories*/
    if(Categories.find().fetch().length == 0)
        promiseInsert(Categories, {key: "bathroom", subCategories: []}, {});
    // promiseInsert(Categories, {key: "bedroom", subCategories: []}, {}).then((res) => {cat2 = res.id});
    // promiseInsert(Categories, {key: "kitchen", subCategories: []}, {}).then((res) => {cat3 = res.id});
    // promiseInsert(Categories, {key: "livingRoom", subCategories: []}, {}).then((res) => {cat4 = res.id});

    /* Adding sample profiles*/

    if(Profiles.find().fetch().length == 0){
        promiseInsert(Profiles, {
                userId: amirId,
                firstName: "Amirhossein",
                lastName: "Nouranizadeh",
                imgUrl: "/images/amir.jpg",
                bio: 'دربارهٔ کودکی و جوانی کوروش و سال‌های اولیهٔ زندگی او روایات متعددی وجود دارد؛ اگرچه هر یک سرگذشت تولد وی را به شرح خاصی نقل کرده‌اند، اما شرحی که آنها دربارهٔ ماجرای زایش کوروش ارائه داده‌اند، بیشتر شبیه افسانه‌است. هرودوت در مورد دستیابی کوروش به قدرت، چهار داستان نقل می‌کند؛ ولی فقط یکی از آنها را معتبر می‌داند. طبق نظر گزنفون از قرن پنجم تا قرن چهارم پیش از میلاد مسیح سلسله داستان‌های متفاوتی دربارهٔ کوروش نقل می‌شده‌است.',
                followers_count: 1412,
                following_count: 1501,
                projects: ['vSrGy26KjNyFTgPFX', 'ifARvrYmcdEq3MKkD'],
                ideaBox: ['vWjxHXoYbxTSw8DH6'],
                badges: [],
                rate: 4.5,
                location: "Iran/Tehran",
                speciality: [],
                contact:{
                    address: 'No.14, Saadi (8) deadend, Abouzar str., Ground floor',
                    tel: '+98 31 32348812',
                    fax: '+98 31 32348813',
                    website: 'www.s-cc.org',
                    email: 'info@s-cc.org'
                },
                socials:[
                    {id: 0, brand: 'facebook', href:'http://www.faacebook.com'},
                    {id: 1, brand: 'twitter', href:'http://www.twitter.com'},
                    {id: 2, brand: 'pinterest', href:'http://www.pinterest.com'}
                ],
                medals:[{id: 0, imgUrl:'/images/assets/medal.png', href:'/', imgAlt:'Medal'}],
                team: ['TbAeSS8f5c6xNSJoo'],
                reviews: []
            }
        );
        promiseInsert(Profiles, {
            userId: mehdiId,
            firstName: "Mehdi",
            lastName: "Balouchi",
            imgUrl: "/images/mehdi.jpg",
            bio: 'دربارهٔ کودکی و جوانی کوروش و سال‌های اولیهٔ زندگی او روایات متعددی وجود دارد؛ اگرچه هر یک سرگذشت تولد وی را به شرح خاصی نقل کرده‌اند، اما شرحی که آنها دربارهٔ ماجرای زایش کوروش ارائه داده‌اند، بیشتر شبیه افسانه‌است. هرودوت در مورد دستیابی کوروش به قدرت، چهار داستان نقل می‌کند؛ ولی فقط یکی از آنها را معتبر می‌داند. طبق نظر گزنفون از قرن پنجم تا قرن چهارم پیش از میلاد مسیح سلسله داستان‌های متفاوتی دربارهٔ کوروش نقل می‌شده‌است.',
            followers_count: 1412,
            following_count: 1501,
            projects: [],
            ideaBox: ['cCuSXusLZdcgDyruK'],
            badges: [],
            rate: 4.5,
            location: "Iran/Tehran",
            speciality: [],
            contact:
            {
                address: 'No.14, Saadi (8) deadend, Abouzar str., Ground floor',
                tel: '+98 31 32348812',
                fax: '+98 31 32348813',
                website: 'www.s-cc.org',
                email: 'info@s-cc.org'
            }
            , socials:[
                {id: 0, brand: 'facebook', href:'http://www.facebook.com'},
                {id: 1, brand: 'twitter', href:'http://www.twitter.com'},
                {id: 2, brand: 'pinterest', href:'http://www.pinterest.com'}
            ], medals:[{id: 0, imgUrl:'/images/assets/medal.png', href:'/', imgAlt:'Medal'}],
            team: ['TbAeSS8f5c6xNSJoo'],
            reviews: []
        });
        promiseInsert(Profiles, {
            userId: raminId,
            firstName: "Ramin",
            lastName: "Barati",
            imgUrl: "/images/ramin.jpg",
            bio: 'دربارهٔ کودکی و جوانی کوروش و سال‌های اولیهٔ زندگی او روایات متعددی وجود دارد؛ اگرچه هر یک سرگذشت تولد وی را به شرح خاصی نقل کرده‌اند، اما شرحی که آنها دربارهٔ ماجرای زایش کوروش ارائه داده‌اند، بیشتر شبیه افسانه‌است. هرودوت در مورد دستیابی کوروش به قدرت، چهار داستان نقل می‌کند؛ ولی فقط یکی از آنها را معتبر می‌داند. طبق نظر گزنفون از قرن پنجم تا قرن چهارم پیش از میلاد مسیح سلسله داستان‌های متفاوتی دربارهٔ کوروش نقل می‌شده‌است.',
            followers_count: 1412,
            following_count: 1501,
            projects: [],
            ideaBox: ['RjWf78FFjro3MTjag'],
            badges: [],
            rate: 4.5,
            location: "Iran/Tehran",
            speciality: [],
            contact:{
                address: 'No.14, Saadi (8) deadend, Abouzar str., Ground floor',
                tel: '+98 31 32348812',
                fax: '+98 31 32348813',
                website: 'www.s-cc.org',
                email: 'info@s-cc.org'
            },
            socials:[
                {id: 0, brand: 'facebook', href:'http://www.facebook.com'},
                {id: 1, brand: 'twitter', href:'http://www.twitter.com'},
                {id: 2, brand: 'pinterest', href:'http://www.pinterest.com'}
            ], medals:[{id: 0, imgUrl:'/images/assets/medal.png', href:'/', imgAlt:'Medal'}],
            team: ['TbAeSS8f5c6xNSJoo'],
            reviews: []
        });
    }

    /* Adding sample photos*/

    if(Photos.find().fetch().length == 0) {
        promiseInsert(Photos, {
            ownerId: amirId,
            imgUrl: "/images/samples/Bedroom/be1.jpg",
            referenceId: "",
            category: []
        });
        promiseInsert(Photos, {
            ownerId: amirId,
            imgUrl: "/images/samples/LivingRoom/l1.jpg",
            referenceId: "",
            category: []
        });
        promiseInsert(Photos, {
            ownerId: amirId,
            imgUrl: "/images/samples/LivingRoom/l2.jpg",
            referenceId: "",
            category: []
        });
        promiseInsert(Photos, {
            ownerId: amirId,
            imgUrl: "/images/samples/LivingRoom/l3.jpg",
            referenceId: "",
            category: []
        });
        promiseInsert(Photos, {
            ownerId: mehdiId,
            imgUrl: "/images/samples/Bedroom/be3.jpg",
            referenceId: "",
            category: []
        });
        promiseInsert(Photos, {
            ownerId: mehdiId,
            imgUrl: "/images/samples/Bedroom/be4.jpg",
            referenceId: "",
            category: []
        });
        promiseInsert(Photos, {
            ownerId: mehdiId,
            imgUrl: "/images/samples/Bedroom/be5.jpg",
            referenceId: "",
            category: []
        });
        promiseInsert(Photos, {
            ownerId: mehdiId,
            imgUrl: "/images/samples/Bedroom/be6.jpg",
            referenceId: "",
            category: []
        });
        promiseInsert(Photos, {
            ownerId: mehdiId,
            imgUrl: "/images/samples/Bedroom/be7.jpg",
            referenceId: "",
            category: []
        })
        promiseInsert(Photos, {
            ownerId: mehdiId,
            imgUrl: "/images/samples/Bedroom/be8.jpg",
            referenceId: "",
            category: []
        })

    }

    /* Adding sample stories */

    if(Stories.find().fetch().length == 0){
        promiseInsert(Stories, {
            ownerId: mehdiId,
            imgUrl: '/images/samples/Medical/m1.jpg',
            title: 'داستان ۱',
            description: 'این ابتکار جالب که روزی به نام پاسدار را ارتباط بدهند با میلاد مسعود سیدالشهداء (سلام الله علیه)، یک ابتکار پرمغز و جهت‌دهی است. زیرا داعیه‌ی سپاه پاسداران، داعیه‌ی بزرگی است: پاسداری از انقلاب اسلامی، این هویت سپاه که هویت پاسداری است، این به معنای یک مفهوم محافظه‌کارانه تلقی نشود.',
            content: '',
            likes: [],
            comments: []
        });
        promiseInsert(Stories, {
            ownerId: amirId,
            imgUrl: '/images/samples/Medical/m2.jpg',
            title: 'داستان 2',
            description: 'این ابتکار جالب که روزی به نام پاسدار را ارتباط بدهند با میلاد مسعود سیدالشهداء (سلام الله علیه)، یک ابتکار پرمغز و جهت‌دهی است. زیرا داعیه‌ی سپاه پاسداران، داعیه‌ی بزرگی است: پاسداری از انقلاب اسلامی، این هویت سپاه که هویت پاسداری است، این به معنای یک مفهوم محافظه‌کارانه تلقی نشود.',
            content: '',
            likes: [],
            comments: []
        });
        promiseInsert(Stories, {
            ownerId: raminId,
            imgUrl: '/images/samples/Medical/m3.jpg',
            title: 'داستان 3',
            description: 'این ابتکار جالب که روزی به نام پاسدار را ارتباط بدهند با میلاد مسعود سیدالشهداء (سلام الله علیه)، یک ابتکار پرمغز و جهت‌دهی است. زیرا داعیه‌ی سپاه پاسداران، داعیه‌ی بزرگی است: پاسداری از انقلاب اسلامی، این هویت سپاه که هویت پاسداری است، این به معنای یک مفهوم محافظه‌کارانه تلقی نشود.',
            content: '',
            likes: [],
            comments: []
        });
    }

    /* Adding sample Discussions*/

    if(Discussions.find().fetch().length == 0){
        promiseInsert(Discussions, {
            ownerId: raminId,
            imgUrl: '/images/samples/Office/o1.jpg',
            title: 'طراحی داخلی در کافه های اصفهان 1',
            content: 'معماری ایران دارای ویژگیهایی است که در مقایسه با معماری کشورهای دیگر جهان از ارزشی ویژه برخوردار است: ویژگیهایی چون طراحی مناسب، محاسبات دقیق، فرم درست پوشش، رعایت مسائل فنی و علمی در ساختمان، ایوانهای رفیع، ستونهای بلند و بالاخره تزئینات گوناگون که هریک در عین سادگی معرف شکوه معماری ایران است',
            createdAt: '12 ساعت پیش',
            comments: []
        });
        promiseInsert(Discussions, {
            ownerId: mehdiId,
            imgUrl: '/images/samples/Office/o2.jpg',
            title: 'طراحی داخلی در کافه های اصفهان 2',
            content: 'معماری ایران دارای ویژگیهایی است که در مقایسه با معماری کشورهای دیگر جهان از ارزشی ویژه برخوردار است: ویژگیهایی چون طراحی مناسب، محاسبات دقیق، فرم درست پوشش، رعایت مسائل فنی و علمی در ساختمان، ایوانهای رفیع، ستونهای بلند و بالاخره تزئینات گوناگون که هریک در عین سادگی معرف شکوه معماری ایران است',
            createdAt: '12 ساعت پیش',
            comments: []
        });
        promiseInsert(Discussions, {
            ownerId: amirId,
            imgUrl: '/images/samples/Office/o3.jpg',
            title: 'طراحی داخلی در کافه های اصفهان3',
            content: 'معماری ایران دارای ویژگیهایی است که در مقایسه با معماری کشورهای دیگر جهان از ارزشی ویژه برخوردار است: ویژگیهایی چون طراحی مناسب، محاسبات دقیق، فرم درست پوشش، رعایت مسائل فنی و علمی در ساختمان، ایوانهای رفیع، ستونهای بلند و بالاخره تزئینات گوناگون که هریک در عین سادگی معرف شکوه معماری ایران است',
            createdAt: '12 ساعت پیش',
            comments: []
        });
        promiseInsert(Discussions, {
            ownerId: raminId,
            imgUrl: '/images/samples/Office/o4.jpg',
            title: 'طراحی داخلی در کافه های اصفهان4',
            content: 'معماری ایران دارای ویژگیهایی است که در مقایسه با معماری کشورهای دیگر جهان از ارزشی ویژه برخوردار است: ویژگیهایی چون طراحی مناسب، محاسبات دقیق، فرم درست پوشش، رعایت مسائل فنی و علمی در ساختمان، ایوانهای رفیع، ستونهای بلند و بالاخره تزئینات گوناگون که هریک در عین سادگی معرف شکوه معماری ایران است',
            createdAt: '12 ساعت پیش',
            comments: []
        });
        promiseInsert(Discussions, {
            ownerId: mehdiId,
            imgUrl: '/images/samples/Office/o5.jpg',
            title: 'طراحی داخلی در کافه های اصفهان5',
            content: 'معماری ایران دارای ویژگیهایی است که در مقایسه با معماری کشورهای دیگر جهان از ارزشی ویژه برخوردار است: ویژگیهایی چون طراحی مناسب، محاسبات دقیق، فرم درست پوشش، رعایت مسائل فنی و علمی در ساختمان، ایوانهای رفیع، ستونهای بلند و بالاخره تزئینات گوناگون که هریک در عین سادگی معرف شکوه معماری ایران است',
            createdAt: '12 ساعت پیش',
            comments: []
        });
        promiseInsert(Discussions, {
            ownerId: amirId,
            imgUrl: '/images/samples/Office/o6.jpg',
            title: 'طراحی داخلی در کافه های اصفهان 6',
            content: 'معماری ایران دارای ویژگیهایی است که در مقایسه با معماری کشورهای دیگر جهان از ارزشی ویژه برخوردار است: ویژگیهایی چون طراحی مناسب، محاسبات دقیق، فرم درست پوشش، رعایت مسائل فنی و علمی در ساختمان، ایوانهای رفیع، ستونهای بلند و بالاخره تزئینات گوناگون که هریک در عین سادگی معرف شکوه معماری ایران است',
            createdAt: '12 ساعت پیش',
            comments: []
        });
    }
    /* Adding sample Comments*/
    if(Comments.find().fetch().length == 0){
        promiseInsert(Comments, {
            ownerId: amirId,
            referenceId: '',
            text: 'طراحی داخلی در کافه های اصفهان',
            likes: [],
            replies: []
        })
        promiseInsert(Comments, {
            ownerId: mehdiId,
            referenceId: '',
            text: 'طراحی داخلی در کافه های اصفهان',
            likes: [],
            replies: []
        })
        promiseInsert(Comments, {
            ownerId: raminId,
            referenceId: '',
            text: 'طراحی داخلی در کافه های اصفهان',
            likes: [],
            replies: []
        })
    }
    /* Adding sample IdeaBoxes */
    if(IdeaBoxes.find().fetch().length == 0){
        promiseInsert(IdeaBoxes, {
            ownerId: raminId,
            photos: ['JMaoAantCbz8LApXh', 'gBySYvyjhtXZ6JmJE', 'aCYmGdy3GRzoPN3Xn', 'Mc8xoWLAenLtfZmKi'],
            title: 'ایده باکس شماره 1',
            likes: [],
            comments: []
        });
        promiseInsert(IdeaBoxes, {
            ownerId: amirId,
            photos: ['L2Jrfkkw9dFfavntN', '9yhEoNqoHLAd96A5A', 'jZuvshahKKNMmMKEf', 'dHG3MTFKPB9uNqTSA'],
            title: 'ایده باکس شماره 2',
            likes: [],
            comments: []
        });
        promiseInsert(IdeaBoxes, {
            ownerId: mehdiId,
            photos: ['RdL8whR4KbB5zPs7B', 'HzBczr2xQ98PXwDTk', 'gBySYvyjhtXZ6JmJE', '9yhEoNqoHLAd96A5A'],
            title: 'ایده باکس شماره 3',
            likes: [],
            comments: []
        });
    }

    /* Adding sample projects*/

    if(Projects.find().fetch().length == 0){
        promiseInsert(Projects, {
            ownerId: amirId,
            title: 'طراحی داخلی واحد مسکونی',
            cost: 24,
            year: '1395',
            name: 'استودیو بام',
            country: 'ایران',
            city: 'اصفهان',
            photos: ['JMaoAantCbz8LApXh', 'aCYmGdy3GRzoPN3Xn', 'L2Jrfkkw9dFfavntN', '9yhEoNqoHLAd96A5A']
        });
        promiseInsert(Projects, {
            ownerId: amirId,
            title: 'طراحی واحد تجاری',
            cost: 107,
            year: '1394',
            name: 'مجتمع تجاری شقایق',
            country: 'ایران',
            city: 'تهران',
            photos: ['jZuvshahKKNMmMKEf', 'dHG3MTFKPB9uNqTSA', 'aCYmGdy3GRzoPN3Xn', 'Mc8xoWLAenLtfZmKi']
        });
    }

    /* Adding sample reviews*/


    if(Reviews.find().fetch().length == 0){
        promiseInsert(Reviews,{
            ownerId: amirId,
            reviewerId: raminId,
            reviewRate: 3.5,
            projectId: 'vSrGy26KjNyFTgPFX',
            content: 'معمار ایرانی توانست وسیع‌ترین دهانه‌ها را با کست افزود پیمون‌ها به وجود بیاورد و آرایش‌های گوناگون و سرگرم‌کننده خلق کند؛ به گونه‌ای که ساختمان دو اشکوب به اندازه‌ای از هم دور شده که گویی اشکوب زیری بعدها بر آن افزوده شده است. معمار ایرانی توانست وسیع‌ترین دهانه‌ها را با کست افزود پیمون‌ها به وجود بیاورد و آرایش‌های گوناگون و سرگرم‌کننده خلق کند؛ به گونه‌ای که ساختمان دو اشکوب به اندازه‌ای از هم دور شده که گویی اشکوب زیری بعدها بر آن افزوده شده است. معمار ایرانی توانست وسیع‌ترین دهانه‌ها را با کست افزود پیمون‌ها به وجود بیاورد و آرایش‌های گوناگون و سرگرم‌کننده خلق کند؛ به گونه‌ای که ساختمان دو اشکوب به اندازه‌ای از هم دور شده که گویی اشکوب زیری بعدها بر آن افزوده شده است. معمار ایرانی توانست وسیع‌ترین دهانه‌ها را با کست افزود پیمون‌ها به وجود بیاورد و آرایش‌های گوناگون و سرگرم‌کننده خلق کند؛ به گونه‌ای که ساختمان دو اشکوب به اندازه‌ای از هم دور شده که گویی اشکوب زیری بعدها بر آن افزوده شده است.'
        });
        promiseInsert(Reviews,{
            ownerId: amirId,
            reviewerId: mehdiId,
            reviewRate: 1.5,
            projectId: 'ifARvrYmcdEq3MKkD',
            content: 'معمار ایرانی توانست وسیع‌ترین دهانه‌ها را با کست افزود پیمون‌ها به وجود بیاورد و آرایش‌های گوناگون و سرگرم‌کننده خلق کند؛ به گونه‌ای که ساختمان دو اشکوب به اندازه‌ای از هم دور شده که گویی اشکوب زیری بعدها بر آن افزوده شده است. معمار ایرانی توانست وسیع‌ترین دهانه‌ها را با کست افزود پیمون‌ها به وجود بیاورد و آرایش‌های گوناگون و سرگرم‌کننده خلق کند؛ به گونه‌ای که ساختمان دو اشکوب به اندازه‌ای از هم دور شده که گویی اشکوب زیری بعدها بر آن افزوده شده است. معمار ایرانی توانست وسیع‌ترین دهانه‌ها را با کست افزود پیمون‌ها به وجود بیاورد و آرایش‌های گوناگون و سرگرم‌کننده خلق کند؛ به گونه‌ای که ساختمان دو اشکوب به اندازه‌ای از هم دور شده که گویی اشکوب زیری بعدها بر آن افزوده شده است. معمار ایرانی توانست وسیع‌ترین دهانه‌ها را با کست افزود پیمون‌ها به وجود بیاورد و آرایش‌های گوناگون و سرگرم‌کننده خلق کند؛ به گونه‌ای که ساختمان دو اشکوب به اندازه‌ای از هم دور شده که گویی اشکوب زیری بعدها بر آن افزوده شده است.'
        });
    }
    /* Adding sample Teams */

    if(Teams.find().fetch().length == 0){
        promiseInsert(Teams,{
            ownerId: amirId,
            name: 'گروه نرم افزاری هوش افزار',
            imgUrl: '/images/samples/exterior/2.png',
            members: [amirId, mehdiId, raminId],
            speciality: ["طراحی داخلی"],
            country: 'ایران',
            city: 'اصفهان',
            projects: [],
            reviews: []
        })
    }
});