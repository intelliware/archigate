/**
 * Created by mehdi on 7/6/16.
 */
import Photos from '../imports/collections/Photos';
import Projects from '../imports/collections/Projects';
import Comments from '../imports/collections/Comments';
import {promiseInsert, promiseUpdate, promiseFind} from '../lib/utils';

let collectionNamesMap = {
    "Photos": Photos,
    "Comments": Comments,
    "Projects": Projects
};

Meteor.methods({
    insertDocument: (collection, document) => {
        return promiseInsert(collectionNamesMap[collection], document);
    },

    updateDocument: (collection, selector, document, options) => {
        return promiseUpdate(collectionNamesMap[collection], selector, document, options);
    },

    findDocument: (collection, query) => {
        return promiseFind(collectionNamesMap[collection], query);
    },

    removeAll: () => {
        for (collection in collectionNamesMap){
            collectionNamesMap[collection].remove({});
        }
    }
});