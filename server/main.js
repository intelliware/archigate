import { Meteor } from 'meteor/meteor';
import {promiseInsert, promiseFind, promiseUpdate, promiseCall} from '../lib/utils';
import Profiles from '../imports/collections/Profiles';
import Photos from '../imports/collections/Photos';

Meteor.startup(() => {
  // code to run on server at startup
    Accounts.onCreateUser(function(options, user) {
        user._id = Meteor.users._makeNewID();
        promiseInsert(Profiles,{userId: user._id, isFinished: false});
        return user;
    });
    

});
