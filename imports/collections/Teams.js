/**
 * Created by Amirhossein on 8/13/2016.
 */
import {Mongo} from 'meteor/mongo';

const Teams = new Mongo.Collection('teams');

Teams.allow({
    insert: () => true

})
if(Meteor.isServer)
    Meteor.publish('Teams', () => {
        return Teams.find();
    });

export default Teams;