import {Mongo} from 'meteor/mongo';

const IdeaBoxes = new Mongo.Collection('ideaBoxes');

IdeaBoxes.allow({
    insert: () => true

})
if(Meteor.isServer)
    Meteor.publish('IdeaBoxes', () => {
        return IdeaBoxes.find();
    });

export default IdeaBoxes;