/**
 * Created by Amirhossein on 7/27/2016.
 */

import {Mongo} from 'meteor/mongo';

const Tags = new Mongo.Collection('tags');

Tags.allow({
    insert: () => true

})
if(Meteor.isServer)
    Meteor.publish('Tags', () => {
        return Tags.find();
    });

export default Tags;