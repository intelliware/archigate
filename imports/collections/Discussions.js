/**
 * Created by Amirhossein on 8/4/2016.
 */
import {Mongo} from 'meteor/mongo';

const Discussions = new Mongo.Collection('discussions');

Discussions.allow({
    insert: () => true
});

if(Meteor.isServer){
    Meteor.publish('Discussions', () => {
        return Discussions.find();
    })
}

export default Discussions;