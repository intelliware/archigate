/**
 * Created by Amirhossein on 8/9/2016.
 */
import {Mongo} from 'meteor/mongo';

const Reviews = new Mongo.Collection('reviews');

Reviews.allow({
    insert: () => true

})
if(Meteor.isServer)
    Meteor.publish('Reviews', () => {
        return Reviews.find();
    });

export default Reviews;