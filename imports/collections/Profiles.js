import {Mongo} from 'meteor/mongo';

const Profiles = new Mongo.Collection('profiles');

Profiles.allow({
    insert: () => true,
    update: () => true

})
if(Meteor.isServer)
    Meteor.publish('Profiles', () => {
        return Profiles.find();
    });

export default Profiles;