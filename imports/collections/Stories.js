import {Mongo} from 'meteor/mongo';

const Stories = new Mongo.Collection('stories');

Stories.allow({
    insert: () => true

})
if(Meteor.isServer)
    Meteor.publish('Stories', () => {
        return Stories.find();
    });

export default Stories;