/**
 * Created by Amirhossein on 7/27/2016.
 */

import {Mongo} from 'meteor/mongo';

const Categories = new Mongo.Collection('categories');

Categories.allow({
    insert: () => true

});
if(Meteor.isServer)
    Meteor.publish('categories', () => {
        return Categories.find();
    });

export default Categories;