import {Mongo} from 'meteor/mongo';

const Projects = new Mongo.Collection('projects');

Projects.allow({
    insert: () => true

})
if(Meteor.isServer)
    Meteor.publish('Projects', () => {
        return Projects.find();
    });

export default Projects;