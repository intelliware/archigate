import {Mongo} from 'meteor/mongo';

const Photos = new Mongo.Collection('photos');

Photos.allow({
    insert: () => true

})
if(Meteor.isServer)
    Meteor.publish('photos', () => {
        return Photos.find();
    });

export default Photos;