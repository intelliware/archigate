/**
 * Created by mehdi on 7/6/16.
 */
import {Mongo} from 'meteor/mongo';

const Comments = new Mongo.Collection('comments');

Comments.allow({
    insert: () => true

})
if(Meteor.isServer)
    Meteor.publish('Comments', () => {
        return Comments.find();
    });

export default Comments;