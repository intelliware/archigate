import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';
import Profiles from '../../../collections/Profiles';
import {promiseUpdate, promiseFind} from '../../../../lib/utils';
import {createContainer} from 'meteor/react-meteor-data';

class ChooseAccountType extends Component {
    componentWillMount() {
        this.state = {
            profile: this.props.profile == [] ? {} : this.props.profile[0]
        }
    }

    componentWillReceiveProps(newProps) {
        this.setState({
            profile: newProps.profile == [] ? {} : newProps.profile[0]
        })
    }

    addAccountType(id, type) {
        Meteor.subscribe('Profiles');
        if(type == 'personal')
            promiseUpdate(Profiles, {_id: id}, {$set: {type: type}}).then((no_affected) => {
                this.context.router.push('/register/personal/step1');
            })
        else
            promiseUpdate(Profiles, {_id: id}, {$set: {type: type}}).then((no_affected) => {
                this.context.router.push('/register/professional/step1');
            })
        
    }

    render() {
        let profile = this.state.profile;
        const T = i18n.createComponent();
        if (profile) {
            return (
                <div className="archi-choose-account-type-container">
                    <button onClick={this.addAccountType.bind(this, profile._id, 'professional')}>
                        <div className="content">
                            <div className="circle border-blue"></div>
                            <div className="text-blue"><T>common.professional</T></div>
                            <div className="text-blue"><T>common.professionalDescription</T></div>
                        </div>
                    </button>
                    <button onClick={this.addAccountType.bind(this, profile._id, 'personal')}>
                        <div className="content">
                            <div className="circle border-yellow"></div>
                            <div className="text-blue"><T>common.personal</T></div>
                            <div className="text-blue"><T>common.personalDescription</T></div>
                        </div>
                    </button>
                </div>
            );
        }
        else
            return null;
    }
}
ChooseAccountType.contextTypes = {
    router: React.PropTypes.object
};
export default createContainer(() =>{
    Meteor.subscribe('Profiles');
    return{
        profile: Profiles.find({userId: Meteor.userId()}).fetch()
    }
}, ChooseAccountType)