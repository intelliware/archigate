import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';
import Profiles from '../../../collections/Profiles';
import {promiseUpdate, promiseFind} from '../../../../lib/utils';
import {createContainer} from 'meteor/react-meteor-data';

class ProfessionalUserSpecificInfoStep2 extends Component{
    componentWillMount() {
        this.state = {
            profile: this.props.profile == [] ? {} : this.props.profile[0]
        }
    }

    componentWillReceiveProps(newProps) {
        this.setState({
            profile: newProps.profile == [] ? {} : newProps.profile[0]
        })
    }
    nextStep(id, e){
        e.preventDefault();
        let speciality = this.refs.speciality.value;
        let businessName = this.refs.businessName.value;
        let activityType = this.refs.activityType.value;
        let businessCity = this.refs.businessCity.value;
        let businessCountry = this.refs.businessCountry.value;
        let feeFrom = this.refs.feeFrom.value;
        let description = this.refs.description.value;
        let feeTo = this.refs.feeTo.value;
        console.log(speciality);
        console.log(businessName);
        console.log(activityType);
        console.log(businessCity);
        console.log(businessCountry);
        console.log(feeFrom);
        console.log(feeTo);
        console.log(description);

        promiseUpdate(Profiles, {_id: id}, {$push: {speciality: speciality}}, {$set: {businessName: businessName, activityType: activityType, businessCity: businessCity, businessCountry: businessCountry, feeFrom: feeFrom,feeTo: feeTo, description: description}}).then((no_affected) => {
            this.context.router.push('/register/finalStep');
        })
    }
    render(){
        let profile = this.state.profile;
        const T = i18n.createComponent();
        if(profile)
            return(
                <div className="archi-wz-container">
                    <div className="stepsGuide">
                        <div className="col-4"><T>common.firstStep</T></div>
                        <div className="col-4"><T>common.secondStep</T></div>
                        <div className="col-4"><T>common.thirdStep</T></div>
                    </div>
                    <form onSubmit={this.nextStep.bind(this, profile._id)}>
                        <fieldset>
                            <legend><T>common.professionalInfo</T></legend>
                            <div className="archi-table">
                                <div className="archi-table-row hastam-container">
                                    <div className="archi-table-cell end">
                                        <label htmlFor="speciality"><T>common.iAm</T></label>:
                                        <input ref="speciality" id="speciality" type="text"/>
                                    </div>
                                    <label className="hastam" htmlFor="hastam"><T>common.hastam</T></label>
                                </div>
                                <div className="archi-table-row">
                                    <div className="archi-table-cell end">
                                        <label htmlFor="businessName"><T>common.name</T> <T>common.company</T>/<T>common.shop</T>/<T>common.brand</T></label>:
                                        <input ref="businessName" id="businessName" type="text"/>
                                    </div>
                                </div>
                                <div className="archi-table-row">
                                    <div className="archi-table-cell end">
                                        <label htmlFor="activityType"><T>common.activityType</T></label>:
                                        <input ref="activityType" id="activityType" type="text"/>
                                    </div>
                                </div>
                                <div className="archi-table-row">
                                    <div className="archi-table-cell end">
                                        <label htmlFor="businessCity"><T>common.city</T></label>:
                                        <input ref="businessCity" id="businessCity" type="text"/>
                                    </div>
                                </div>
                                <div className="archi-table-row">
                                    <div className="archi-table-cell end">
                                        <label htmlFor="businessCountry"><T>common.country</T></label>:
                                        <input ref="businessCountry" id="businessCountry" type="text"/>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend><T>common.feeRange</T></legend>
                            <div className="archi-table">
                                <div className="archi-table-row">
                                    <div className="archi-table-cell end">
                                        <label htmlFor="feeFrom"><T>common.from</T></label>:
                                        <input ref="feeFrom" id="feeFrom" type="text"/>
                                    </div>
                                    <div className="archi-table-cell end">
                                        <label htmlFor="feeTo"><T>common.to</T></label>:
                                        <input ref="feeTo" id="feeTo" type="text"/>
                                    </div>
                                    <div className="archi-table-cell end">
                                        <label htmlFor="firstName"><T>common.toman</T></label>
                                    </div>
                                </div>
                                <div className="archi-table-row">
                                    <div className="archi-table-cell textarea">
                                        <label htmlFor="description"><T>common.description</T>:</label>
                                        <textarea id='description' ref="description" rows="3"/>
                                    </div>
                                </div>
                                <div className="archi-table-row">
                                    <div className="archi-table-cell end">
                                        <button type="submit" className="btn btn-yellow"><T>common.nextStep</T></button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            );
        else return null;

    }

}
ProfessionalUserSpecificInfoStep2.contextTypes = {
    router: React.PropTypes.object
};
export default createContainer(() =>{
    Meteor.subscribe('Profiles');
    return{
        profile: Profiles.find({userId: Meteor.userId()}).fetch()
    }
}, ProfessionalUserSpecificInfoStep2)