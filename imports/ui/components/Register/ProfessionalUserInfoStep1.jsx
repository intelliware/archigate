import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';
import Profiles from '../../../collections/Profiles';
import {promiseUpdate, promiseFind} from '../../../../lib/utils';
import {createContainer} from 'meteor/react-meteor-data';

class ProfessionalUserInfoStep1 extends Component{
    componentWillMount() {
        this.state = {
            profile: this.props.profile == [] ? {} : this.props.profile[0]
        }
    }

    componentWillReceiveProps(newProps) {
        this.setState({
            profile: newProps.profile == [] ? {} : newProps.profile[0]
        })
    }
    nextStep(id, e){
        e.preventDefault();
        let firstName = this.refs.firstName.value;
        let lastName = this.refs.lastName.value;
        let gender = this.refs.male.checked == true ? 'male' : 'female';
        let birthdayDate = this.refs.birthdayDate.value;
        let city = this.refs.city.value;
        let country = this.refs.country.value;
        let address = this.refs.address.value;
        let defaultLanguage = this.refs.defaultLanguage.value;
        let about = this.refs.about.value;
        promiseUpdate(Profiles, {_id: id}, {$set: {firstName: firstName, lastName: lastName, gender: gender, birthdayDate: birthdayDate, city: city, country: country, address: address, defaultLanguage: defaultLanguage, about: about}}).then((no_affected) => {
            this.context.router.push('/register/professional/step2');
        })




    }
    render(){
        const T = i18n.createComponent();
        let profile = this.state.profile;
        if(profile)
            return (
                <div className="archi-wz-container">
                    <div className="stepsGuide">
                        <div className="col-4"><T>common.firstStep</T></div>
                        <div className="col-4"><T>common.secondStep</T></div>
                        <div className="col-4"><T>common.thirdStep</T></div>
                    </div>
                    <form onSubmit={this.nextStep.bind(this, profile._id)}>
                        <fieldset>
                            <legend><T>common.personalInfo</T></legend>
                            <div className="archi-table">
                                <div className="archi-table-row">
                                    <div className="archi-table-cell">
                                        <label htmlFor="firstName"><T>common.name</T></label>:
                                        <input ref="firstName" id="firstName" type="text"/>
                                    </div>
                                    <div className="archi-table-cell end">
                                        <label htmlFor="lastName"><T>common.lastName</T></label>:
                                        <input ref="lastName" id="lastName" type="text"/>
                                    </div>
                                </div>
                                <div className="archi-table-row">
                                    <div className="archi-table-cell">
                                        <div className="gender">
                                            <input id="male" ref="male" type="radio" name="gender" value="male"/>
                                            <label htmlFor="male"><T>common.male</T></label>
                                            <input id="female" ref="female" type="radio" name="gender" value="female"/>
                                            <label htmlFor="female"><T>common.female</T></label>
                                        </div>
                                    </div>
                                    <div className="archi-table-cell end">
                                        <label htmlFor="birthdayDate"><T>common.birthdayDate</T></label>:
                                        <input ref="birthdayDate" id="birthdayDate" type="text"/>
                                    </div>
                                </div>
                                <div className="archi-table-row">
                                    <div className="archi-table-cell end">
                                        <label htmlFor="defaultLanguage"><T>common.chooseLang</T></label>:
                                        <input ref="defaultLanguage" id="defaultLanguage" type="text"/>
                                    </div>
                                </div>
                                <div className="archi-table-row">
                                    <div className="archi-table-cell textarea">
                                        <label htmlFor="about"><T>common.aboutYourself</T>:</label>
                                        <textarea id='about' ref="about" type="text" rows="3"/>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend><T>common.address</T></legend>
                            <div className="archi-table">
                                <div className="archi-table-row">
                                    <div className="archi-table-cell">
                                        <label htmlFor="city"><T>common.city</T></label>:
                                        <input ref="city" id="city" type="text"/>
                                    </div>
                                    <div className="archi-table-cell end">
                                        <label htmlFor="country"><T>common.country</T></label>:
                                        <input ref="country" id="country" type="text"/>
                                    </div>
                                </div>
                                <div className="archi-table-row">
                                    <div className="archi-table-cell textarea">
                                        <label htmlFor="address"><T>common.address</T>:</label>
                                        <textarea id='address' ref="address" type="text" rows="3"/>
                                    </div>
                                </div>
                                <div className="archi-table-row">
                                    <div className="archi-table-cell">
                                        <label htmlFor="zipCode"><T>common.zipCode</T></label>:
                                        <input ref="zipCode" id="zipCode" type="text"/>
                                    </div>
                                </div>
                                <div className="archi-table-row">
                                    <div className="archi-table-cell end">
                                        <button type="submit" className="btn btn-yellow"><T>common.nextStep</T></button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            );
        else return null;
    }
}
ProfessionalUserInfoStep1.contextTypes = {
    router: React.PropTypes.object
};
export default createContainer(() =>{
    Meteor.subscribe('Profiles');
    return{
        profile: Profiles.find({userId: Meteor.userId()}).fetch()
    }
}, ProfessionalUserInfoStep1)