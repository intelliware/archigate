import React, {Component} from 'react';
import Profiles from '../../../collections/Profiles';
import {promiseUpdate} from '../../../../lib/utils';
import {createContainer} from 'meteor/react-meteor-data';

class PersonalUserSpecificInfoStep2 extends Component{
    componentWillMount() {
        this.state = {
            profile: this.props.profile == [] ? {} : this.props.profile[0],
            futureActivities: [],
            favoriteGenre: []
        }
    }

    componentWillReceiveProps(newProps) {
        this.setState({
            profile: newProps.profile == [] ? {} : newProps.profile[0],
            futureActivities: [],
            favoriteGenre: []
        })
    }
    onChange(id,type, event){
        let array = this.state[type].slice(0);
        if(event.target.checked == true)
            array.push(id)
        else
            for(let i = 0; i<array.length; i++)
                if(array[i] == id)
                    array.splice(i, 1)
        this.setState({[type]: array});

    }
    nextStep(id, e){
        e.preventDefault();
        let futureActivities = this.state.futureActivities;
        let favoriteGenre = this.state.favoriteGenre;
        promiseUpdate(Profiles, {_id: id}, {$set: {futureActivities: futureActivities, favoriteGenre: favoriteGenre}}).then((no_affected) => {
            this.context.router.push('/register/finalStep');
        })
    }
    render(){
        const T = i18n.createComponent();
        let profile = this.state.profile;
        if(profile)
            return (
                <div className="archi-wz-container">
                    <div className="stepsGuide">
                        <div className="col-4"><T>common.firstStep</T></div>
                        <div className="col-4"><T>common.secondStep</T></div>
                        <div className="col-4"><T>common.thirdStep</T></div>
                    </div>
                    <form onSubmit={this.nextStep.bind(this, profile._id)}>
                        <fieldset>
                            <legend><T>common.futurePurposesQuestion</T></legend>
                            <div className="question">
                                <div className="archi-table">
                                    <div className="archi-table-row">
                                        <div className="archi-table-cell">
                                            <div className="checkbox-radio">
                                                <input ref='buildNewHouse' onChange={this.onChange.bind(this, "buildNewHouse", "futureActivities")} type="checkbox" id="buildNewHouse" name="buildNewHouse" value="Build New House" />
                                                <label htmlFor="buildNewHouse"><T>common.buildNewHouse</T></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="archi-table-row">
                                        <div className="archi-table-cell">
                                            <div className="checkbox-radio">
                                                <input ref='rebuildingView' onChange={this.onChange.bind(this, "rebuildingView", "futureActivities")} type="checkbox" id="rebuildingView" name="rebuildingView" value="Rebuilding View" />
                                                <label htmlFor="rebuildingView"><T>common.rebuildingView</T></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="archi-table-row">
                                        <div className="archi-table-cell">
                                            <div className="checkbox-radio">
                                                <input ref='rebuildingInteriorSpace' onChange={this.onChange.bind(this, "rebuildingInteriorSpace", "futureActivities")} type="checkbox" id="rebuildingInteriorSpace" name="rebuildingInteriorSpace" value="Rebuilding Interior Space" />
                                                <label htmlFor="rebuildingInteriorSpace"><T>common.rebuildingInteriorSpace</T></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="archi-table">
                                    <div className="archi-table-row">
                                        <div className="archi-table-cell">
                                            <div className="checkbox-radio">
                                                <input ref='layoutAndDecoration' onChange={this.onChange.bind(this, "layoutAndDecoration", "futureActivities")} type="checkbox" id="layoutAndDecoration" name="layoutAndDecoration" value="Layout And Decoration" />
                                                <label htmlFor="layoutAndDecoration"><T>common.layoutAndDecoration</T></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="archi-table-row">
                                        <div className="archi-table-cell">
                                            <div className="checkbox-radio">
                                                <input ref='rebuildingGreenSpace' onChange={this.onChange.bind(this, "rebuildingGreenSpace", "futureActivities")} type="checkbox" id="rebuildingGreenSpace" name="rebuildingGreenSpace" value="Rebuilding Green Space" />
                                                <label htmlFor="rebuildingGreenSpace"><T>common.rebuildingGreenSpace</T></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="archi-table-row">
                                        <div className="archi-table-cell">
                                            <div className="checkbox-radio">
                                                <input ref='completingHomeEquipment' onChange={this.onChange.bind(this, "completingHomeEquipment", "futureActivities")} type="checkbox" id="completingHomeEquipment" name="completingHomeEquipment" value="Completing Home Equipment" />
                                                <label htmlFor="completingHomeEquipment"><T>common.completingHomeEquipment</T></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <fieldset>
                                <legend><T>common.chooseFavoriteGenre</T></legend>
                                <div className="question">
                                    <div className="archi-table">
                                        <div className="archi-table-row">
                                            <div className="archi-table-cell">
                                                <div className="checkbox-radio">
                                                    <input ref='contemporary' onChange={this.onChange.bind(this, "contemporary", "favoriteGenre")} type="checkbox" id="contemporary" name="contemporary" value="contemporary" />
                                                    <label htmlFor="contemporary"><T>categories.contemporary</T></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="archi-table-row">
                                            <div className="archi-table-cell">
                                                <div className="checkbox-radio">
                                                    <input ref='traditional' onChange={this.onChange.bind(this, "traditional", "favoriteGenre")} type="checkbox" id="traditional" name="traditional" value="traditional" />
                                                    <label htmlFor="traditional"><T>categories.traditional</T></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="archi-table-row">
                                            <div className="archi-table-cell">
                                                <div className="checkbox-radio">
                                                    <input ref='classic' onChange={this.onChange.bind(this, "classic", "favoriteGenre")} type="checkbox" id="classic" name="classic" value="classic" />
                                                    <label htmlFor="classic"><T>categories.classic</T></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="archi-table">
                                        <div className="archi-table-row">
                                            <div className="archi-table-cell">
                                                <div className="checkbox-radio">
                                                    <input ref='modern' onChange={this.onChange.bind(this, "modern", "favoriteGenre")} type="checkbox" id="modern" name="modern" value="modern" />
                                                    <label htmlFor="modern"><T>categories.modern</T></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="archi-table-row">
                                            <div className="archi-table-cell">
                                                <div className="checkbox-radio">
                                                    <input ref='islamic' onChange={this.onChange.bind(this, "islamic", "favoriteGenre")} type="checkbox" id="islamic" name="islamic" value="islamic" />
                                                    <label htmlFor="islamic"><T>categories.islamic</T></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="archi-table-row">
                                            <div className="archi-table-cell">
                                                <div className="checkbox-radio">
                                                    <input ref='rural' onChange={this.onChange.bind(this, "rural", "favoriteGenre")} type="checkbox" id="rural" name="rural" value="rural" />
                                                    <label htmlFor="rural"><T>categories.rural</T></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="archi-table-row">
                                            <div className="archi-table-cell end">
                                                <button type="submit" className="btn btn-yellow"><T>common.nextStep</T></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </fieldset>
                    </form>
                </div>
            );
        else return null;
    }
}
PersonalUserSpecificInfoStep2.contextTypes = {
    router: React.PropTypes.object
};
export default createContainer(() =>{
    Meteor.subscribe('Profiles');
    return{
        profile: Profiles.find({userId: Meteor.userId()}).fetch()
    }
}, PersonalUserSpecificInfoStep2)