import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';
import Profiles from '../../../collections/Profiles';
import {promiseUpdate, promiseFind} from '../../../../lib/utils';
import {createContainer} from 'meteor/react-meteor-data';

class FinalStep extends Component{
    componentWillMount() {
        this.state = {
            profile: this.props.profile == [] ? {} : this.props.profile[0]
        }
    }

    componentWillReceiveProps(newProps) {
        this.setState({
            profile: newProps.profile == [] ? {} : newProps.profile[0]
        })
    }
    nextStep(id, e){
        // e.preventDefault();
        // let firstName = this.refs.firstName.value;
        // let lastName = this.refs.lastName.value;
        // let gender = this.refs.male.checked == true ? 'male' : 'female';
        // let birthdayDate = this.refs.birthdayDate.value;
        // let city = this.refs.city.value;
        // let country = this.refs.country.value;
        // let address = this.refs.address.value;
        // promiseUpdate(Profiles, {_id: id}, {$set: {firstName: firstName, lastName: lastName, gender: gender, birthdayDate: birthdayDate, city: city, country: country, address: address}}).then((no_affected) => {
        //     this.context.router.push('/register/personal/step2');
        // })
    }
    render(){
        const T = i18n.createComponent();
        return(
            <div>sege</div>
        );
    }
}
FinalStep.contextTypes = {
    router: React.PropTypes.object
};
export default createContainer(() =>{
    Meteor.subscribe('Profiles');
    return{
        profile: Profiles.find({userId: Meteor.userId()}).fetch()
    }
}, FinalStep)