import React, {Component, PropTypes} from 'react';

class NotificationButton extends Component {
    renderBubble(){
        return (
            <span>
                {this.props.count}
                <span></span>
            </span>
        );
    }

    render() {
        let fa = 'fa fa-' + this.props.icon;
        let classes = 'archi-notification-btn' + (this.props.count < 1 ? ' mute' : '');
        
        return (
            <button className={classes} title={this.props.tool}>
                <i className={fa}></i>
                {this.props.count ? this.renderBubble() : null}
            </button>
        );
    }
}

NotificationButton.propTypes = {
    icon: PropTypes.string.isRequired,
    count: PropTypes.number.isRequired,
    tooltip: PropTypes.string.isRequired,
};

export default NotificationButton;