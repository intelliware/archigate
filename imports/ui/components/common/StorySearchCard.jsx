import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';
import Profiles from '../../../collections/Profiles';
import {createContainer} from 'meteor/react-meteor-data';

class StorySearchCard extends Component{
    constructor(props){
        super(props);
        this.state = {
            profile: null
        }
    }
    componentWillMount(){
        this.setState({
            profile: this.props.profile[0]
        })
    }
    componentWillReceiveProps(newProps){
        this.setState({
            profile: newProps.profile[0]
        })

    }
    render(){
        const T = i18n.createComponent();
        let profile = this.state.profile;
        let story = this.props.storyObject;
        if(profile != null)
        return (
            <div className="archi-row story-card-container">
                <div className="col-4 archi-no-padding">
                    <a href="/">
                        <img src={story.imgUrl} />
                    </a>
                </div>
                <div className="col-8">
                    <div className="body">
                        <h3>{story.title}</h3>
                        <p>{story.description}</p>
                    </div>
                    <div className="archi-row footer">
                        <div className="col-2">
                            <span><i className="fa fa-heart-o"></i></span>
                        </div>
                        <div className="col-2">
                            <span><i className="fa fa-comment-o"></i></span>
                        </div>
                        <div className="col-2">
                            <span><i className="fa fa-circle-thin"></i></span>
                        </div>
                        <div className="col-6">
                            <span>
                                <T>common.writtenBy</T> <a href="/">{profile.firstName}</a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        )
        else return null
    }
}
export default createContainer((params) => {
    Meteor.subscribe('Profiles');
    return{
        profile: Profiles.find({userId: params.storyObject.ownerId}).fetch()
    };
}, StorySearchCard)