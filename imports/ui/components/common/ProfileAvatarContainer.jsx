import React, {Component} from 'react';
import {Link} from 'react-router';
import Profiles from '../../../collections/Profiles';
import {createContainer} from 'meteor/react-meteor-data';

class ProfileAvatarContainer extends Component{
    componentWillMount(){
        this.state = {
            profile: this.props.profile == [] ? {} : this.props.profile[0]
        }
    }
    componentWillReceiveProps(newProps){
        this.setState({
            profile: newProps.profile == [] ? {} : newProps.profile
        })
    }
    render(){
        let profile = this.state.profile;
        if(profile){
            return(
                <div className="archi-profile-avatar">
                    <Link to={'/profile/' + profile.userId + '/'}><img src={profile.imgUrl} alt=""/></Link>
                </div>
            );
        }
        else return null;
    }
}
export default createContainer((params) => {
    Meteor.subscribe('Profiles');
    return {
        profile: Profiles.find({userId: params.profileId}).fetch()
    }
},ProfileAvatarContainer)