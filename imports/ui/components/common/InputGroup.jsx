import React, {Component, PropTypes} from 'react';

class InputGroup extends Component {
    constructor(props){
        super(props);
        this.state = {
            placeholder: this.props.placeholder || '',
            label: this.props.label
        }
    }
    render() {
        let {placeholder, label} = this.state;
        return (
            <div className="archi-input-group">
                <span>
                    <label type="button">{label}</label>
                </span>
                <input type="text" placeholder={placeholder} />
            </div>
        );
    }
}

InputGroup.propTypes = {
    placeholder: React.PropTypes.string,
    label: React.PropTypes.string.isRequired
};

export default InputGroup;