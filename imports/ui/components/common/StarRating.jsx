import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';

class StarsRating extends Component {
    constructor(props){
        super(props);
        this.state = {isRTL: i18n.isRTL()}
    }

    _localeChanged(locale){
        this.setState({isRTL: i18n.isRTL(locale)});
    }

    componentWillMount(){
        i18n.onChangeLocale(this._localeChanged.bind(this));
    }

    componentWillUnmount(){
        i18n.offChangeLocale(this._localeChanged.bind(this));
    }

    render() {
        let rating = this.props.rating;
        let max = this.props.max;
        let stars = [];
        for (var i = 0; i < max; i++) {
            let d = rating - i;
            if(d >= 1){
                stars.push({id: i,class: 'star'});
                continue;
            }
            if(d < 1 && d >= 0.5){
                stars.push({id: i,class: 'star-half-o'});
                continue;
            }
            if(d < 0.5){
                stars.push({id: i,class: 'star-o'});
            }
        }

        return(
            <span>
                {stars.map((star) => {
                    let starClass = 'fa fa-' + star.class + (this.state.isRTL ? ' fa-flip-horizontal' : ''); 
                    return (<i key={star.id} className={starClass}></i>);
                })}
            </span>
        );
    }
}


export default StarsRating;