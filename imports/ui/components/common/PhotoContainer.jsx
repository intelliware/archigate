import React, {Component} from 'react';
import { Link } from 'react-router';
import i18n from 'meteor/universe:i18n';
import Profiles from '../../../../imports/collections/Profiles'
import { createContainer } from 'meteor/react-meteor-data';

class PhotoContainer extends Component{
    // constructor(props){
    //     super(props);
    //     this.state = {profile: null};
    // }
    //@TODO
    // fetchPhotoObject(photoId){
    //
    //     let photos = [
    //         {
    //             url: '/images/samples/Bedroom/be1.jpg',
    //             href: 'photos/1',
    //             category: 'Bathroom',
    //             user:{
    //                 imgURL: '/images/assets/fullFakhari.jpg',
    //                 route: '/profile/1',
    //                 name: 'Amin Fakhari',
    //                 group: 'Arman Insurance Exterior'
    //             }
    //         },
    //         {
    //             url: '/images/samples/LivingRoom/l1.jpg',
    //             href: 'photos/1',
    //             category: 'Bathroom',
    //             user:{
    //                 imgURL: '/images/assets/fullFakhari.jpg',
    //                 route: '/profile/1',
    //                 name: 'Amin Fakhari',
    //                 group: 'Arman Insurance Exterior'
    //             }
    //         },
    //         {
    //             url: '/images/samples/LivingRoom/l2.jpg',
    //             href: 'photos/1',
    //             category: 'Bathroom',
    //             user:{
    //                 imgURL: '/images/assets/fullFakhari.jpg',
    //                 route: '/profile/1',
    //                 name: 'Amin Fakhari',
    //                 group: 'Arman Insurance Exterior'
    //             }
    //         },
    //         {
    //             url: '/images/samples/LivingRoom/l3.jpg',
    //             href: 'photos/1',
    //             category: 'Bathroom',
    //             user:{
    //                 imgURL: '/images/assets/fullFakhari.jpg',
    //                 route: '/profile/1',
    //                 name: 'Amin Fakhari',
    //                 group: 'Arman Insurance Exterior'
    //             }
    //         },
    //         {
    //             url: '/images/samples/Bedroom/be2.jpg',
    //             href: 'photos/1',
    //             category: 'Bathroom',
    //             user:{
    //                 imgURL: '/images/assets/fullFakhari.jpg',
    //                 route: '/profile/1',
    //                 name: 'Amin Fakhari',
    //                 group: 'Arman Insurance Exterior'
    //             }
    //         },
    //         {
    //             url: '/images/samples/Bedroom/be3.jpg',
    //             href: 'photos/1',
    //             category: 'Bathroom',
    //             user:{
    //                 imgURL: '/images/assets/fullFakhari.jpg',
    //                 route: '/profile/1',
    //                 name: 'Amin Fakhari',
    //                 group: 'Arman Insurance Exterior'
    //             }
    //         },
    //         {
    //             url: '/images/samples/Bedroom/be4.jpg',
    //             href: 'photos/1',
    //             category: 'Bathroom',
    //             user:{
    //                 imgURL: '/images/assets/fullFakhari.jpg',
    //                 route: '/profile/1',
    //                 name: 'Amin Fakhari',
    //                 group: 'Arman Insurance Exterior'
    //             }
    //         },
    //         {
    //             url: '/images/samples/Bedroom/be5.jpg',
    //             href: 'photos/1',
    //             category: 'Bathroom',
    //             user:{
    //                 imgURL: '/images/assets/fullFakhari.jpg',
    //                 route: '/profile/1',
    //                 name: 'Amin Fakhari',
    //                 group: 'Arman Insurance Exterior'
    //             }
    //         },
    //         {
    //             url: '/images/samples/Bedroom/be6.jpg',
    //             href: 'photos/1',
    //             category: 'Bathroom',
    //             user:{
    //                 imgURL: '/images/assets/fullFakhari.jpg',
    //                 route: '/profile/1',
    //                 name: 'Amin Fakhari',
    //                 group: 'Arman Insurance Exterior'
    //             }
    //         },
    //         {
    //             url: '/images/samples/Bedroom/be7.jpg',
    //             href: 'photos/1',
    //             category: 'Bathroom',
    //             user:{
    //                 imgURL: '/images/assets/fullFakhari.jpg',
    //                 route: '/profile/1',
    //                 name: 'Amin Fakhari',
    //                 group: 'Arman Insurance Exterior'
    //             }
    //         }
    //     ];
    //     return photos[photoId];
    // }
    // componentWillMount(){
    //     this.state.photoObject = this.fetchPhotoObject(this.props.photoId);
    //     let photoObject = this.props.photo;
    // }


    render(){
        let profile = this.props.profile;
        let photoObject = this.props.photo;
        let componentSize = this.props.componentSize || 'small';
        let containerCssClass = 'archi-photo-container ' + componentSize;
        const T = i18n.createComponent();
        if(photoObject == null)
            return (<h3 className="">Null photoObject</h3>);
        if(profile != null){
            return (
                <div className={containerCssClass}>
                    <a href={'/photos/' + photoObject._id}>
                        <img className="photo" src={photoObject.imgUrl} alt={photoObject.category}/>
                        <div>{photoObject.category}</div>
                    </a>
                    <div className="toolbar">
                        <div className="archi-row">
                            <div className="col vert-middle">
                                <div className="user-details">
                                    <Link to={'/profile/' + profile.userId + '/'}><img src={profile.imgUrl} alt={profile.firstName}/></Link>
                                    <a href={'/profile/' + profile.userId + '/'}><span>{profile.firstName + ' ' +profile.lastName}</span></a>
                                </div>
                            </div>
                            <div className="col vert-middle">
                                <div className="actions">
                                    <button>
                                    <span className="fa-stack">
                                        <i className="fa fa-circle-thin fa-stack-2x"></i>
                                        <i className="fa fa-plus fa-stack-1x"></i>
                                    </span>
                                    </button>
                                    <button><i className="fa fa-heart-o fa-2x"></i></button>
                                </div>
                            </div>
                        </div>
                        <div>
                            {/*photoObject.user.group*/}
                        </div>
                    </div>
                </div>
            );
        } else return(<div>loading</div>);
    }
}
export default createContainer((params) => {
    Meteor.subscribe('Profiles');
    // console.log(params.photo.ownerId);
    // let p = Profiles.find({userId: params.photo.ownerId}).fetch()[0];
    // console.log(p ? p.firstName:'null');
    return {
        profile: params.photo ? Profiles.find({userId: params.photo.ownerId}).fetch()[0] : {}
    };
}, PhotoContainer);

// <div className="archi-row">
//                         <div className="col-6">
//                             <ul>
//                                 <li><a className="btn"><T>common.follow</T></a></li>
//                                 <li><a className="btn"><T>common.review</T></a></li>
//                             </ul>
//                         </div>
//                         <div className="col-6">
//                             <ul className="archi-align-right">
//                                 <li>AMIN FAKHARI</li>
//                                 <li>طراح داخلی</li>
//                                 <li>15 REVIEWS</li>
//                             </ul>
//                         </div>
//                     </div>