import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';
import PhotoContainer from './PhotoContainer.jsx';
export default class PhotoRow4 extends Component {
    render(){
        return(
            <div className="archi-row archi-4-photo-row">
                <div className="col-3">
                    <PhotoContainer photoId="1" componentSize="small" />
                </div>
                <div className="col-3">
                    <PhotoContainer photoId="2" componentSize="small" />
                </div>
                <div className="col-3">
                    <PhotoContainer photoId="3" componentSize="small" />
                </div>
                <div className="col-3">
                    <PhotoContainer photoId="4" componentSize="small" />
                </div>
            </div>
        )
    }
}