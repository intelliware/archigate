import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';


export default class RelatedPhotos extends Component{
    fetchPhotos(scope){
        let photos = {
            'project': [
                {
                    url: '/images/samples/Bedroom/be1.jpg',
                    href: '/images/samples/Bedroom/be5.jpg'
                },
                {
                    url: '/images/samples/Bedroom/be2.jpg',
                    href: '/images/samples/Bedroom/be5.jpg'
                },
                {
                    url: '/images/samples/Bedroom/be3.jpg',
                    href: '/images/samples/Bedroom/be5.jpg'
                }
            ],
            'category': [
                {
                    url: '/images/samples/Bedroom/be1.jpg',
                    href: '/images/samples/Bedroom/be5.jpg'
                },
                {
                    url: '/images/samples/Bedroom/be2.jpg',
                    href: '/images/samples/Bedroom/be5.jpg'
                },
                {
                    url: '/images/samples/Bedroom/be3.jpg',
                    href: '/images/samples/Bedroom/be5.jpg'
                }
            ]
        };

        return photos[scope];
    }

    renderPhotos(){
        let scope = this.props.scope;
        return this.fetchPhotos(scope).map((photo) => {
            return (
                <div className="col-4">
                    <a href={photo.href}><img src={photo.url} /></a>
                </div>
            );
        })
    }

    render(){
        const T = i18n.createComponent();
        let header = this.props.scope == "project" ? (<T>common.fromProject</T>) : (<T></T>);
        let caption = this.props.scope == "project" ? this.props.image.caption : this.props.image.category;
        return(
            <div className="archi-row archi-related-photos-container">
                <div className="header">
                    <h4><T>common.otherPhotos</T><span>{header} </span>{caption}</h4>
                </div>
                {this.renderPhotos()}
            </div>
        );
    }
}