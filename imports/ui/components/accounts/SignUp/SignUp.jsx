import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';
import {passwordIsValid, emailIsValid} from '/imports/utility/validation'

export class SignUp extends Component{

    submit(e){
        e.preventDefault();
        let email = this.refs.registerEmail.value;
        let password = this.refs.registerPassword.value;

        let validated = true;

        if(!emailIsValid(email)){
            //TODO: Inform the user of a valid email
            validated = false;
        }

        if(!passwordIsValid(password)){
            //TODO: Inform the user of a valid password
            validated = false;
        }

        if(validated) {
            Accounts.createUser({email: email, password: password}, function(err){
                //TODO: Inform the user of the error
                console.log(err);
            });
            this.context.router.push('/register');
        }

        //TODO: redirect the user to home

        return false;
    }

    render(){
        const T = i18n.createComponent();
        return (
            <div className="row archi-inner-container signup">
                <div className="col-md-4 col-xs-12 archi-no-padding">
                    <form onSubmit={this.submit.bind(this)}>
                        <div className="form-group">
                            <input ref="registerEmail" id="register-email" className="form-control" type="email"/>
                            <label for="register-email"><T>accounts.email</T></label>
                        </div>
                        <div className="form-group">
                            <input ref="registerPassword" id="register-password" className="form-control" type="password"/>
                            <label for="register-password"><T>accounts.password</T></label>
                        </div>
                        <div>
                            <button className="btn" type="submit"><T>accounts.signUp</T></button>
                        </div>
                    </form>
                </div>
                <div className="col-md-4 hidden-xs"></div>
                <div className="col-md-4 col-xs-12 archi-oauth-container signup archi-no-padding">
                    <div>
                        <a href="">
                            <span><img src="/images/assets/14.png"/></span>
                            <T>accounts.googleLoginMessage</T>
                        </a>
                    </div>
                    <div>
                        <a href="">
                            <span><img src="/images/assets/13.png"/></span>
                            <T>accounts.linkedInLoginMessage</T>
                        </a>
                    </div>
                    <div>
                        <p><T>accounts.privacyPolicyMessage</T></p>
                    </div>
                </div>
            </div>
        )
    }
}
SignUp.contextTypes = {
    router: React.PropTypes.object
};
export default SignUp;