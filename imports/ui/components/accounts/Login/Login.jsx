import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';
import {passwordIsValid, emailIsValid} from '/imports/utility/validation'

export default class Login extends Component{
    submit(e){
        e.preventDefault();
        let email = this.refs.loginEmail.value;
        let password = this.refs.loginPassword.value;

        let validated = true;

        if(!emailIsValid(email)){
            //TODO: Inform the user of a valid email
            validated = false;
        }

        if(!passwordIsValid(password)){
            //TODO: Inform the user of a valid password
            validated = false;
        }

        let that = this;

        if(validated) {
            Meteor.loginWithPassword(email, password, function (err) {
                //TODO: Inform the user of the error
                if(err){
                    console.log(err);
                    return false;
                }
                console.log(self);

                const { location } = that.props
                
                if (location.state && location.state.nextPathname) {
                    that.context.router.push(location.state.nextPathname);
                } else {
                    that.context.router.push('/profile/' + Meteor.userId());
                }
            });
        }
        return false;
    }

    render(){
        const T = i18n.createComponent();
        return (
            <div className="row archi-inner-container login">
                <div className="col-md-4 col-xs-12 archi-no-padding">
                    <form onSubmit={this.submit.bind(this)}>
                        <div className="form-group">
                            <input ref="loginEmail" id="login-email" className="form-control" type="email"/>
                            <label for="login-email"><T>accounts.email</T></label>
                        </div>
                        <div className="form-group">
                            <input ref="loginPassword" id="login-password" className="form-control" type="password"/>
                            <label for="login-password"><T>accounts.password</T></label>
                        </div>
                        <div>
                            <button className="btn" type="submit"><T>accounts.login</T></button>
                            <div>
                                <input type="checkbox"/><T>accounts.rememberMe</T>
                            </div>
                        </div>
                    </form>
                </div>
                <div className="col-md-4 hidden-xs"></div>
                <div className="col-md-4 col-xs-12 archi-oauth-container login archi-no-padding">
                    <div>
                        <a href="">
                            <span><img src="/images/assets/14.png"/></span>
                            <T>accounts.googleLoginMessage</T>
                        </a>
                    </div>
                    <div>
                        <a href="">
                            <span><img src="/images/assets/13.png"/></span>
                            <T>accounts.linkedInLoginMessage</T>
                        </a>
                    </div>
                    <div>
                        <h4><T>accounts.notRegisteredMessage</T></h4>
                        <p><T>accounts.rightNow</T><a href="/accounts/signup"><T>accounts.register</T></a></p>
                    </div>
                    <div>
                        <p><T>accounts.privacyPolicyMessage</T></p>
                    </div>
                </div>
            </div>
        )
    }
}

Login.contextTypes = {
  router: React.PropTypes.object.isRequired
}

