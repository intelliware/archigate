import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';
import Profiles from '../../../collections/Profiles';
import StarRating from '../common/StarRating.jsx';
import Projects from '../../../collections/Projects';
import {createContainer} from 'meteor/react-meteor-data';



class ProfileOthersReviewsOverviewCard extends Component{
    componentWillReceiveProps(newProps){
        this.setState({
            profile: newProps.profile.length == 0 ? {} : newProps.profile[0],
            project: newProps.project.length == 0 ? {} : newProps.project[0]
        })
    }
    componentWillMount(){
        this.state = {
            profile: this.props.profile[0] == undefined ? {} : this.props.profile[0],
            project: this.props.project[0] == undefined ? {} : this.props.project[0]
        }
    }
    render(){
        const T = i18n.createComponent();
        let profile = this.state.profile;
        let project = this.state.project;

        let review = this.props.review;
        return(
          <div className="archi-othersReviews-overViewCard-container">
              <div className="reviewer-info">
                  <div>
                      <img src={profile.imgUrl} alt=""/>
                      <span>{profile.firstName + ' ' + profile.lastName}</span>
                  </div>
                  <div>
                      {profile.location}
                  </div>
              </div>
              <div className="star-rating">
                  <StarRating rating={review.reviewRate} max={5} />
              </div>
              <div className="project-info">
                  <h4 className="yellow"><T>projects.projectTitle</T> :</h4>
                  <h4>{project.title}</h4>
                  <h4 className="yellow"><T>projects.projectDate</T> :</h4>
                  <h4>{project.year}</h4>
                  <h4 className="yellow"><T>projects.projectCost</T> :</h4>
                  <h4>{project.cost} <T>common.IRCurrency</T></h4>
              </div>
              <div className="content">
                  {review.content}
              </div>
          </div>
        );
    }
}
export default createContainer((params) =>{
    Meteor.subscribe('Profiles');
    Meteor.subscribe('Projects');
    console.log(Projects.find({_id: params.review.projectId}).fetch());
    return {
        profile: Profiles.find({userId: params.review.reviewerId}).fetch(),
        project: Projects.find({_id: params.review.projectId}).fetch()
    }
}, ProfileOthersReviewsOverviewCard);