import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';
import Stories from '../../../collections/Stories';
import Discussions from '../../../collections/Discussions';
import StorySearchCard from '../common/StorySearchCard.jsx';
import DiscussionSearchCard from '../Search/DiscussionSearchCard.jsx';
import {createContainer} from 'meteor/react-meteor-data';


class ProfileGapboxSection extends Component{
    constructor(props){
        super(props);
        this.state = {
            stories: [],
            discussions: []
        }
    }
    componentWillReceiveProps(newProps){
        this.setState({
            stories: newProps.stories == null ? null : newProps.stories,
            discussions: newProps.discussions == null ? null : newProps.discussions
        })
    }

    render(){
        let stories = this.state.stories;
        let discussions = this.state.discussions;
        console.log(stories);
        if(stories.length != 0)
        return(
            <div className="archi-profile-gapbox-container">
                {
                    stories.map((story, index) => {
                        return <StorySearchCard key={index} storyObject={story} />

                })}
                {
                    discussions.map((discussion, index) => {
                        console.log(discussion);
                        return <DiscussionSearchCard key={index} discussion={discussion} />

                    })}
            </div>
        );
        else return null;
    }
}
export default createContainer((params) =>{
    Meteor.subscribe('Stories');
    Meteor.subscribe('Discussions');
    return{
        stories: Stories.find({ownerId: params.params.id}).fetch(),
        discussions: Discussions.find({ownerId: params.params.id}).fetch()
    }
}, ProfileGapboxSection)
