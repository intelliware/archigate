import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';
import IdeaBoxes from '/imports/collections/IdeaBoxes';
import ProfileIdeaBoxOverviewCard from './ProfileIdeaBoxOverviewCard.jsx';
import { createContainer } from 'meteor/react-meteor-data';


class ProfileIdeaBoxSection extends Component{
    constructor(props){
        super(props);
        this.state = {
            ideaBoxes: []
        }
    }

    componentWillReceiveProps(newProps){
        this.setState({
            ideaBoxes: newProps.ideaBoxes == null ? null : newProps.ideaBoxes
        })
    }
    // componentWillMount() {
    //     this.state = {
    //         projects: this.fetchProjects()
    //     }
    // }
    // fetch(){
    //     let projects = [
    //         {author: 'امین فخاری', title: 'طراحی داخلی واحد مسکونی', year: '1393', imgUrl: {mainPhoto: '/images/mainphoto.png', sidePhoto1: '/images/sidephoto01.png', sidePhoto2: '/images/sidephoto02.png', sidePhoto3: '/images/sidephoto03.png'}, location: {country: 'ایران', city: 'اصفهان', name: 'استودیو بام' }},
    //         {author: 'امین فخاری', title: 'طراحی داخلی واحد مسکونی', year: '1393', imgUrl: {mainPhoto: '/images/mainphoto.png', sidePhoto1: '/images/sidephoto01.png', sidePhoto2: '/images/sidephoto02.png', sidePhoto3: '/images/sidephoto03.png'}, location: {country: 'ایران', city: 'اصفهان', name: 'استودیو بام' }},
    //         {author: 'امین فخاری', title: 'طراحی داخلی واحد مسکونی', year: '1393', imgUrl: {mainPhoto: '/images/mainphoto.png', sidePhoto1: '/images/sidephoto01.png', sidePhoto2: '/images/sidephoto02.png', sidePhoto3: '/images/sidephoto03.png'}, location: {country: 'ایران', city: 'اصفهان', name: 'استودیو بام' }}
    //     ];
    //     return projects;
    // }
    // renderProjects(project, index){
    //
    //     const T = i18n.createComponent();
    //     return(
    //         <div key={index} className="archi-project-overview-card">
    //             <div>
    //                 <img src={project.imgUrl.mainPhoto} alt="main photo"/>
    //                 <div>
    //                     <div>
    //                         <p>{project.title}</p>
    //                         <span>{project.location.country} {project.location.city}</span> /<span>{project.year}</span> /<span>{project.location.name}</span>
    //                     </div>
    //                     <button className="btn"><T>common.yourReview</T></button>
    //                 </div>
    //             </div>
    //             <ul className="horz-list">
    //                 <li><img src={project.imgUrl.sidePhoto1} alt="side photo 1"/></li>
    //                 <li><img src={project.imgUrl.sidePhoto2} alt="side photo 2"/></li>
    //                 <li><img src={project.imgUrl.sidePhoto3} alt="side photo 3"/></li>
    //             </ul>
    //         </div>
    //     );
    // }

    render(){
        return(
            <div className="archi-profile-projects-container">
                {this.state.ideaBoxes.map((ideaBox, index) => (
                    <ProfileIdeaBoxOverviewCard ideaBox={ideaBox} />
                ))}
            </div>
        );
    }
}

export default createContainer((params) => {
    Meteor.subscribe('IdeaBoxes');
    return {
        ideaBoxes: IdeaBoxes.find({ownerId: params.params.id}).fetch()
    }
}, ProfileIdeaBoxSection);