import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';
import Photos from '/imports/collections/Photos';
import { createContainer } from 'meteor/react-meteor-data';


class ProfileIdeaBoxOverviewCard extends  Component{
    constructor(props){
        super(props);
        this.state = {
            photos: []
        }       
    }
    componentWillReceiveProps(newProps){
        this.setState({
            photos: newProps.photos == null ? []: newProps.photos
        })
    }

    render(){
        const T = i18n.createComponent();
        let ideaBox = this.props.ideaBox;
        let photos = this.state.photos;
        return (
            <div className="archi-profile-ideabox-overview-card">
                <div className="col-9">
                    {
                        photos.map((photo)=>{
                            if(photo == undefined)
                                return;
                            return (
                                <div className="col-3">
                                    <img src={photo.imgUrl} alt=""/>
                                </div>
                            )
                        })
                    }
                </div>
                <div className="col-3">
                    <div>{ideaBox.title}</div>
                    <div></div>
                    <div>
                        <button className="btn btn-blue"><T>common.followUp</T></button>
                    </div>
                </div>
            </div>
        )
    }
}
export default createContainer((params) => {
    Meteor.subscribe('photos');
    return {
        photos: params.ideaBox.photos.map((photoId) => {
            return Photos.find({_id: photoId}).fetch()[0];
        })
    }
}, ProfileIdeaBoxOverviewCard);