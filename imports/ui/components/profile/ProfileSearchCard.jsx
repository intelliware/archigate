import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';
import StarRating from '../common/StarRating.jsx';

export default class ProfileSearchCard extends Component{
    // componentWillMount(){
    //     this.user = {
    //         name: 'امین فخاری',
    //         category: 'categories.interiorDesigner',
    //         location: 'locations.IranTehran',
    //         imageUrl: '/images/assets/fullFakhari.jpg',
    //         reviewsCount: '15',
    //         followers: 12512,
    //         rate: 3.75,
    //         photos: [
    //             {
    //                 src: '/images/samples/Kitchen/k2.jpg',
    //                 href: '/images/samples/Kitchen/k2.jpg'
    //             },
    //             {
    //                 src: '/images/samples/Kitchen/k3.jpg',
    //                 href: '/images/samples/Kitchen/k3.jpg'
    //             },
    //             {
    //                 src: '/images/samples/Kitchen/k4.jpg',
    //                 href: '/images/samples/Kitchen/k4.jpg'
    //             }
    //         ]
    //     };
    // }
    render(){

        const T = i18n.createComponent();
        let pro = this.props.user;
        if(pro != null)
            return (
                <div className="archi-profile-search-card-container">
                    <div className="archi-row">
                        <div className="col-6">
                            <h4>{pro.firstName + " " + pro.lastName}</h4>
                            <div>
                                <span><T>{pro.speciality}</T></span>
                                /
                                <span><T>{pro.location}</T></span>
                            </div>
                            <div>
                                <span className="archi-yellow"><StarRating rating={pro.rate} max="5" /></span>
                                <span>  {/*this.props.user.reviewsCount*/} </span><T>common.reviews</T>
                            </div>
                            <div>
                                <i className="fa fa-users"></i> {pro.followers_count}
                            </div>
                            <div>
                                <button className="btn btn-white"><T>common.yourReview</T></button>
                                <button className="btn btn-white"><T>common.follow</T></button>
                            </div>
                        </div>
                        <div className="col-6">
                            <img src={pro.imgUrl} />
                        </div>
                    </div>
                    {/*<div className="archi-row">
                     <div className="col-4 archi-no-padding">
                     <img src={this.user.photos[0].src} alt="image 1"/>
                     </div>
                     <div className="col-4 archi-no-padding">
                     <img src={this.user.photos[1].src} alt="image 2"/>
                     </div>
                     <div className="col-4 archi-no-padding">
                     <img src={this.user.photos[2].src} alt="image 3"/>
                     </div>
                     </div>*/}
                </div>
            );
        else return null
    }
}