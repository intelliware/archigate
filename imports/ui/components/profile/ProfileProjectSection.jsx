import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';
import ProfileProjectOverviewCard from './ProfileProjectOverviewCard.jsx';
import Projects from '../../../collections/Projects';
import {createContainer} from 'meteor/react-meteor-data';

class ProfileProjectSection extends Component{
    componentWillMount() {
        this.state = {
            projects: []
        }
    }
    componentWillReceiveProps(newProps){
        this.setState({
            projects: newProps.projects == null ? null : newProps.projects
        })
    }
    // fetchProjects(){
    //     let projects = [
    //         {author: 'امین فخاری', title: 'طراحی داخلی واحد مسکونی', year: '1393', imgUrl: {mainPhoto: '/images/mainphoto.png', sidePhoto1: '/images/sidephoto01.png', sidePhoto2: '/images/sidephoto02.png', sidePhoto3: '/images/sidephoto03.png'}, location: {country: 'ایران', city: 'اصفهان', name: 'استودیو بام' }},
    //         {author: 'امین فخاری', title: 'طراحی داخلی واحد مسکونی', year: '1393', imgUrl: {mainPhoto: '/images/mainphoto.png', sidePhoto1: '/images/sidephoto01.png', sidePhoto2: '/images/sidephoto02.png', sidePhoto3: '/images/sidephoto03.png'}, location: {country: 'ایران', city: 'اصفهان', name: 'استودیو بام' }},
    //         {author: 'امین فخاری', title: 'طراحی داخلی واحد مسکونی', year: '1393', imgUrl: {mainPhoto: '/images/mainphoto.png', sidePhoto1: '/images/sidephoto01.png', sidePhoto2: '/images/sidephoto02.png', sidePhoto3: '/images/sidephoto03.png'}, location: {country: 'ایران', city: 'اصفهان', name: 'استودیو بام' }}
    //     ];
    //     return projects;
    // }
    // renderProjects(project, index){
    //
    //     const T = i18n.createComponent();
    //     return(
    //         <div key={index} className="archi-project-overview-card">
    //             <div>
    //                 <img src={project.imgUrl.mainPhoto} alt="main photo"/>
    //                 <div>
    //                     <div>
    //                         <p>{project.title}</p>
    //                         <span>{project.location.country} {project.location.city}</span> /<span>{project.year}</span> /<span>{project.location.name}</span>
    //                     </div>
    //                     <button className="btn"><T>common.yourReview</T></button>
    //                 </div>
    //             </div>
    //             <ul className="horz-list">
    //                 <li><img src={project.imgUrl.sidePhoto1} alt="side photo 1"/></li>
    //                 <li><img src={project.imgUrl.sidePhoto2} alt="side photo 2"/></li>
    //                 <li><img src={project.imgUrl.sidePhoto3} alt="side photo 3"/></li>
    //             </ul>
    //         </div>
    //     );
    // }

    render(){
        let projects = this.state.projects;
        return(
            <div className="archi-profile-projects-container">
                {projects.map((project, index) => (
                    <ProfileProjectOverviewCard key={index} project={project} />
                ))}
            </div>
        );
    }
}
export default createContainer((params) => {
    Meteor.subscribe('Projects');
    return {
        projects: Projects.find({ownerId: params.params.id}).fetch()
    }
}, ProfileProjectSection);