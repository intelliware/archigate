import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';
import {Link, IndexLink} from 'react-router';


export default class ProfilePageNavbarEdit extends Component{
    
    componentWillMount(){
        this.currnetUserId = this.props.profile.userId;
    }
    getMenuItems(){
        return [
            {id: 0, titleI18nKey: 'common.project', route: '/profile/' + this.currnetUserId + '/'},
            {id: 1, titleI18nKey: 'common.ideaBox', route: '/profile/' + this.currnetUserId + '/ideaBox'},
            {id: 2, titleI18nKey: 'common.merchandise',route: '/profile/' + this.currnetUserId + '/merchandise'},
            {id: 3, titleI18nKey: 'common.othersReviews', route: '/profile/' + this.currnetUserId + '/otherReviews'},
            {id: 4, titleI18nKey: 'common.gapBox', route: '/profile/' + this.currnetUserId + '/gapBox'},
            {id: 5, titleI18nKey: 'common.contacts', route: '/profile/' + this.currnetUserId + '/contacts'},
            {id: 6, titleI18nKey: 'common.followUp', route: '/profile/' + this.currnetUserId + '/followUp'},
            {id: 7, titleI18nKey: 'common.message', route: '/profile/' + this.currnetUserId + '/message'}
        ];
    }

    renderMenuItems(){
        const T = i18n.createComponent();
        return this.getMenuItems().map((item, index) => {
            if(item.id == 0)
                return (
                    <li key={index}>
                        <IndexLink to={item.route} activeClassName="selected">
                            <T>{item.titleI18nKey}</T>
                        </IndexLink>
                    </li>
                );
            return (
                <li key={index}>
                    <Link to={item.route} activeClassName="selected">
                        <T>{item.titleI18nKey}</T>
                    </Link>
                </li>
            );
        })
    }

    render(){
        return(
            <div>
                <div className="archi-profile-page-navbar">
                    <ul>
                        {this.renderMenuItems()}
                    </ul>
                </div>
            </div>
        );
    }
}