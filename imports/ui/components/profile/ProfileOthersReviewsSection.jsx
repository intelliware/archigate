import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';
import ProfileOthersReviewsOverviewCard from './ProfileOthersReviewsOverviewCard.jsx';
import Reviews from '../../../collections/Reviews';
import {createContainer} from 'meteor/react-meteor-data';


class ProfileOthersReviewsSection extends Component{
    constructor(props){
        super(props);
        this.state = {
            reviews: []
        }
    }
    componentWillReceiveProps(newProps){
        this.setState({
            reviews: newProps.reviews == null ? null : newProps.reviews
        })
    }
    render(){
        let reviews = this.state.reviews;
        return(
          <div className="archi-profile-othersReviews-container">
              {
                  reviews.map((review, index) => {
                      return <ProfileOthersReviewsOverviewCard key={index} review={review} />
                  })
              }
          </div>
        );
    }
}
export default createContainer((params) => {
    Meteor.subscribe('Reviews');
    return {
        reviews: Reviews.find({ownerId: params.params.id}).fetch()
    }
}, ProfileOthersReviewsSection);