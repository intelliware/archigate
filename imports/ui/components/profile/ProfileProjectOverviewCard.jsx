import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';
import Photos from '../../../collections/Photos';
import {createContainer} from 'meteor/react-meteor-data';

class ProfileProjectOverviewCard extends Component{
    constructor(props){
        super(props);
        this.state = {
            photos: []
        }
    }
    componentWillReceiveProps(newProps){
        this.setState({
            photos: newProps.photos == null ? []: newProps.photos
        })
    }
    render(){
        const T = i18n.createComponent();
        let photos = this.state.photos;
        let project = this.props.project;
        if(!photos[0] || !photos[1] || !photos[2] || !photos[3])
            return null;
        if(photos.length != 0)
            return(
                <div className="archi-project-overview-card">
                    <div>
                        <img src={photos[0].imgUrl} alt="main photo"/>
                        <div>
                            <div>
                                <p>{project.title}</p>
                                <span>{project.country} {project.city}</span> /<span>{project.year}</span> /<span>{project.name}</span>
                            </div>
                            <button className="btn"><T>common.yourReview</T></button>
                        </div>
                    </div>
                    <ul className="horz-list">
                        <li><img src={photos[1].imgUrl} alt="side photo 1"/></li>
                        <li><img src={photos[2].imgUrl} alt="side photo 2"/></li>
                        <li><img src={photos[3].imgUrl} alt="side photo 3"/></li>
                    </ul>
                </div>
            );
        else return null;
    }
}
export default createContainer((params) => {
    Meteor.subscribe('photos');
    return {
        photos: params.project.photos.map((photoId) => {
            return Photos.find({_id: photoId}).fetch()[0];
        })
    }
}, ProfileProjectOverviewCard);