import React, {Component} from 'react';
import { Link } from 'react-router';
import i18n from 'meteor/universe:i18n';

import StarRating from '../common/StarRating.jsx'

class ProfileHead extends Component{
    // componentWillMount(){
    //     this.state = {
    //         profile: this.props.profile.length == 0 ? {} : this.props.profile[0]
    //     }
    // }
    //
    //
    // componentWillReceiveProps(newProps){
    //     this.setState({
    //         profile: newProps.profile[0] == undefined ? {} : newProps.profile[0]
    //     })
    // }

    renderMedals(medals){
        if(medals)
            return medals.map((medal) => {
                return (
                    <li className="archi-medal" key={medal.id}>
                        <Link to={medal.href}>
                            <img src={medal.imgUrl} alt={medal.imgAlt}/>
                        </Link>
                    </li>
                );
            });
    }

    renderBadges(badges){
        if(badges)
            return badges.map((badge) => {
                return (
                    <li className="archi-badge" key={badge.id}>
                        <Link to={badge.href}>
                            <img src={badge.imgUrl} alt={badge.imgAlt}/>
                        </Link>
                    </li>
                );
            });
    }

    renderSocials(socials){
        if(socials)
            return socials.map((social) => {
                let faClass = 'fa fa-' + social.brand + '-square';
                return (
                    <li key={social.id}>
                        <a href={social.href}>
                            <i className={faClass} />
                        </a>
                    </li>);
            });
    }

    render() {
        let profile = this.props.profile;
        if(!profile)
            return null;
        console.log('shodes');
        let fullname = profile.firstName + ' ' + profile.lastName;
        const T = i18n.createComponent();
        return (
            <div className="archi-row archi-profile-head">
                <div className="col-3">
                    <img className="profile-pic" src={profile.imgUrl} alt={fullname}/>
                </div>
                <div className="archi-row col-9">
                    <div className="col-7">
                        <ul className="medals horz-list">
                            {this.renderMedals(profile.medals)}
                        </ul>
                        <p className="profile-name"><strong>{fullname}</strong></p>
                        <p className="profile-description">{profile.location}</p>
                        <ul className="badges horz-list">
                            {this.renderBadges(profile.badges)}
                        </ul>
                        <p className="rating">
                            <StarRating rating={profile.rate} max={5} />  <T>common.reviews</T>
                        </p>
                    </div>
                    <div className="col-5">
                        <ul className="vert-list">
                            <li>{ profile.contact.address }</li>
                            <li><T number={profile.contact.tel}>common.tel</T></li>
                            <li><T number={profile.contact.fax}>common.fax</T></li>
                            <li>{ profile.contact.website }</li>
                            <li>{ profile.contact.email }</li>
                            <li>
                                <ul className="horz-list social">{ this.renderSocials(profile.socials) }</ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

ProfileHead.PropTypes = {
    profile: React.PropTypes.object.isRequired
}

export default ProfileHead;