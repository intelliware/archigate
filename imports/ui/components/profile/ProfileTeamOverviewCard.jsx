import React, {Component} from "react";
import i18n from 'meteor/universe:i18n';
import ProfileAvatarContainer from '../common/ProfileAvatarContainer.jsx';
import Teams from "../../../collections/Teams";
import {createContainer} from "meteor/react-meteor-data";

class ProfileTeamOverviewCard extends Component{
    componentWillMount(){
        this.state = {
            team: this.props.team == [] ? {} : this.props.team[0]
        }
    }
    componentWillReceiveProps(newProps){
        this.setState({
            team: newProps.team == [] ? {} : newProps.team[0]
        })
    }
    render(){
        const T = i18n.createComponent();
        let team = this.state.team;
        if(team){
            return (
                <div className="archi-team-overview-card-container">
                    <div className="col-2 team-img">
                        <img src={team.imgUrl} alt=""/>
                    </div>
                    <div className="col-3 team-info">
                        <div>{team.name}</div>
                        <div>{team.speciality[0]}</div>
                        <div>{team.country + '،' + team.city}</div>
                        <div>reviews</div>
                        <div className="buttons">
                            <button className="btn btn-blue"><T>common.followUp</T></button>
                            <button className="btn btn-blue"><T>common.yourReview</T></button>
                        </div>
                    </div>
                    <div className="col-7 team-members">
                        {
                            team.members.map((member, index) => {
                                return <ProfileAvatarContainer profileId={member} key={index} />
                            })
                        }
                    </div>
                </div>
            );
        }
        else return null;
    }
}

export default createContainer((params) => {
    Meteor.subscribe('Teams');
    return {
        team: Teams.find({_id: params.teamId[0]}).fetch()
    }
},ProfileTeamOverviewCard)