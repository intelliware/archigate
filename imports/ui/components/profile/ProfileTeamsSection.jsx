import React, {Component} from "react";
import ProfileTeamOverviewCard from './ProfileTeamOverviewCard.jsx';
import Profiles from "../../../collections/Profiles";
import {createContainer} from "meteor/react-meteor-data";


class ProfileTeamsSection extends Component{
    componentWillMount(){
        this.state = {
            profile: this.props.profile == [] ? {} : this.props.profile[0]
        }
    }
    componentWillReceiveProps(newProps){
        this.setState({
            profile: newProps.profile == [] ? {} : newProps.profile[0]
        })
    }
    renderTeams(teams){
        return (
            teams.map((team, index) =>{
                return <ProfileTeamOverviewCard key={index} teamId={teams} />
            })
        )
    }
    render(){
        let profile = this.state.profile;
        if(profile){
            let teams = profile.team;
            return (
                <div className="archi-team-section-container">
                    {this.renderTeams(teams)}
                </div>
            );
        }
        else return null;
    }
}
export default createContainer((params) => {
    Meteor.subscribe('Profiles');
    return {
        profile: Profiles.find({userId: params.params.id}).fetch()
    }
}, ProfileTeamsSection);