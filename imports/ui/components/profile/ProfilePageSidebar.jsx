import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';


export default class ProfilePageSidebar extends Component{

    render(){
        const T = i18n.createComponent();
        return(
            <div className="archi-profile-page-sidebar">
                <div>
                    <button className="btn btn-blue"><T>common.follow</T></button>
                    <button className="btn btn-grey"><T>common.sendMessage</T></button>
                </div>
                <div className="second-button-row">
                    <button className="btn btn-blue"><T>common.yourReview</T></button>
                    <button className="btn btn-grey"><T>common.catalog</T></button>
                </div>
                <div className="counts">
                    <span><i className="fa fa-users"> <T count={this.props.profile.followers_count}>common.followersCount</T></i></span>
                    <span className="second-child"><i className="fa fa-users"> <T count={this.props.profile.following_count}>common.followingCount</T></i></span>
                    <span><i className="fa fa-eye"> <T count={this.props.profile.view_count}>common.viewsCount</T></i></span>
                </div>
                <div className="bio">
                    <p><T>common.kosSher</T></p>
                </div>
            </div>
        );
    }
}