/**
 * Created by mehdi on 7/3/16.
 */
import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';
import StarRating from '../common/StarRating.jsx';


export default class ProfileCard extends Component{
    constructor(){
        super();
        this.user = {};
    }

    componentWillMount(){
        this.user = {
            name: 'امین فخاری',
            category: 'categories.interiorDesigner',
            location: 'locations.IranTehran',
            imageUrl: '/images/assets/fullFakhari.jpg',
            reviewsCount: '15',
            rate: 3.75
        };
    }

    render(){
        const T = i18n.createComponent();
        return (
            <div className="archi-profile-card-container archi-row">
                <div className="col-2 arch">
                    <img src={this.user.imageUrl} alt={this.user.name}/>
                </div>
                <div className="col-5">
                    <div>
                        <strong>{this.user.name}</strong>
                    </div>
                    <div>
                        <span><T>{this.user.category}</T></span>
                        /
                        <span><T>{this.user.location}</T></span>
                    </div>
                </div>
                <div className="col-5">
                    <div>
                        <span><StarRating rating={this.user.rate} max="5" /></span>
                        <span>  {this.user.reviewsCount} </span><T>common.reviews</T>
                    </div>
                </div>
            </div>
        );
    }
}