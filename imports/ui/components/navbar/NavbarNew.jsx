import React, { Component } from 'react';
import NavbarMenu from './NavbarMenu.jsx';
import AvatarContainer from './Avatar.jsx';
import NotificationPanelContainer from './NotificationPanel';
import i18n from 'meteor/universe:i18n';
import {Link} from 'react-router';

import reactMixin from 'react-mixin';
import {ReactMeteorData} from 'meteor/react-meteor-data';

export default class NavbarNew extends Component{
    getMeteorData(){
        return{
            currentUser: Meteor.user()
        }
    }

    searchToggle(){
        $('#nav-search').focusout(()=>{
            $('#nav-login').removeClass('animated fadeOutRight');
            $('#nav-login').addClass('animated fadeInRight');
        }).focusin(()=>{
            $('#nav-login').addClass('animated fadeOutRight');
        }).focus();
    }

    logout(e){
        e.preventDefault();
        Meteor.logout();
    }

    renderAuthenticated(){
        return <button onClick={this.logout.bind(this)}>Logout</button>;
    }

    renderAnonymous(){
        const T = i18n.createComponent();
        return (
            <Link to="/accounts" className="pull-right">
                <img src="/images/login.png" alt={i18n.__('common.login')} className=""/>
                <div className="">
                    <T>common.enterYourProfile</T>
                </div>
            </Link>
        );
    }
    changeLang(){
        if(i18n.getLocale()=='en-US')
            i18n.setLocale('fa-IR');
        else
            i18n.setLocale('en-US');
    }

    render(){
        const T = i18n.createComponent();
        let currentUser = this.data.currentUser;
        return (
            <div className="archi-row archi-navbar">
                <div className="col-2">
                    {/*<AvatarContainer />*/}
                </div>
                <div className="col-8">
                    <NavbarMenu />
                </div>
                <div className="col-2">
                    <Link to="/">
                        <img src="/images/logo.png" alt="ArchiGate Logo"/>
                    </Link>
                </div>
            </div>
        );
    }
}

reactMixin(NavbarNew.prototype, ReactMeteorData);
