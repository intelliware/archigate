import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';
import {Link} from 'react-router';


export default class NavbarMenu extends Component{
    getMenuItems(){
        return [
            {id: 0, image: '/images/photos.png', titleI18nKey: 'common.photos', route: '/search'},
            {id: 1, image: '/images/pros.png', titleI18nKey: 'common.pros', route: '/search/professionals'},
            {id: 2, image: '/images/shop.png', titleI18nKey: 'common.shop', route: '/shop'},
            {id: 3, image: '/images/discussions.png', titleI18nKey: 'common.discussions', route: '/search/discussions'},
            {id: 4, image: '/images/stories.png', titleI18nKey: 'common.stories', route: '/search/stories'},
            {id: 5, image: '/images/categories.png', titleI18nKey: 'common.categories', route: '/categories'},
        ];
    }
    render(){
        return (
            <ul className="horz-list">
                <li style={{position: 'relative'}}>
                    <div className="archi-before-search"></div>
                    <input type="text" className="archi-navbar-search"/>
                </li>
                {this.renderMenuItems()}
            </ul>
        );
    }

    renderMenuItems(){
        const T = i18n.createComponent();
        return this.getMenuItems().map((item) => {
            return (
                <li key={item.id}>
                    <Link to={item.route}>
                        <img src={item.image} alt={i18n.__(item.titleI18nKey)} />
                        <div className="nav-label">
                            <T>{item.titleI18nKey}</T>
                        </div>
                    </Link>
                </li>
            );
        });
    }
}