import React, {Component} from 'react';
import {Link} from 'react-router';

import NotificationButton from '../common/NotificationButton.jsx';

// Meteor data imports
import { createContainer } from 'meteor/react-meteor-data';

class NotificationPanel extends Component {
    render() {
        if(!this.props.isAuthenticated)
            return null;
        
        return (
            <div className="archi-notification-panel">
                <ul className="horz-list">
                    <li><NotificationButton icon="users" tooltip="Requests" count={10} /></li>
                    <li><NotificationButton icon="bell" tooltip="Notifications" count={207}/></li>
                    <li><NotificationButton icon="commenting" tooltip="Messages" count={3}/></li>                    
                </ul>
            </div>
        );
    }
}

NotificationPanel.propTypes = {
    isAuthenticated: React.PropTypes.bool,
}

export default NotificationPanelContainer = createContainer(({ params }) => {
  const isAuthenticated = !!Meteor.userId();
  return {
    isAuthenticated
  };
}, NotificationPanel);