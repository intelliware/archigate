import React, {Component} from 'react';
import {Link} from 'react-router';

// Meteor data imports
import { createContainer } from 'meteor/react-meteor-data';

//TODO: Need to find a way to logout, prolly a drop down
class Avatar extends Component {
    getUserData(){
        return {
            name: 'Amin',
            imgUrl: 'http://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg',
            profileUri: Meteor.userId()
        }
    }

    renderAuthenticated(){
        let { name, imgUrl, profileUri } = this.getUserData();
        let profileRoute = '/profile/' + profileUri;

        return (
            <Link to={profileRoute} className="archi-avatar">
                <span>{name}</span>
                <img src={imgUrl} alt="Avatar"/>
            </Link>
        );
    }

    renderAnonymous(){
        const T = i18n.createComponent();
        return (
            <Link to="/accounts" className="pull-right">
                <img src="/images/login.png" alt={i18n.__('common.login')} className=""/>
                <div className="">
                    <T>common.enterYourProfile</T>
                </div>
            </Link>
        );
    }

    render() {
        if(this.props.isAuthenticated)
            return this.renderAuthenticated();
        
        return this.renderAnonymous();
    }
}

Avatar.propTypes = {
    isAuthenticated: React.PropTypes.bool
};

export default AvatarContainer = createContainer(({ params }) => {
  const isAuthenticated = !!Meteor.userId();
  return {
    isAuthenticated
  };
}, Avatar);
