import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n'

export default class DiscussionEditor extends Component{

    render(){
        const T = i18n.createComponent();
        return(
            <div className="archi-discussion-editor-container">
                <form>
                    <div className="title">
                        <label for="title"><T>common.title</T></label>
                        <input id="title" type="text"/>
                    </div>
                    <div className="problem-statement">
                        <label for="problemStatement"><T>common.problemStatement</T></label>
                        <textarea id="problemStatement" rows="5" />
                    </div>
                    <div className="editor-toolbar">
                        <button className="btn btn-yellow" type="submit"><T>common.send</T></button>
                        <div></div>
                        <ul className="horz-list">
                            <li><button className="btn"><i className="fa fa-bold"/></button></li>
                            <li><button className="btn"><i className="fa fa-italic"/></button></li>
                            <li><button className="btn"><i className="fa fa-list-ul"/></button></li>
                            <li><button className="btn"><i className="fa fa-list-ol"/></button></li>
                            <li><button className="btn"><i className="fa fa-camera"/></button></li>
                        </ul>

                    </div>
                </form>
            </div>
        );
    }
}