import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';
import StorySearchCard from '../common/StorySearchCard.jsx';
import PhotoContainer from '../common/PhotoContainer.jsx';
import PhotoRow4 from '../common/PhotoRow4.jsx';
import Stories from '../../../../imports/collections/Stories';
import { createContainer } from 'meteor/react-meteor-data';

class StoriesSearch extends Component{
    // fetchStories(){
    //     let stories = [
    //         {
    //             title: 'داستان ۱',
    //             imageUrl: '/images/samples/Medical/m1.jpg',
    //             href: '/images/b1',
    //             author: {
    //                 name: 'امین فخاری',
    //                 url: '#'
    //             },
    //             description: 'این ابتکار جالب که روزی به نام پاسدار را ارتباط بدهند با میلاد مسعود سیدالشهداء (سلام الله علیه)، یک ابتکار پرمغز و جهت‌دهی است. زیرا داعیه‌ی سپاه پاسداران، داعیه‌ی بزرگی است: پاسداری از انقلاب اسلامی، این هویت سپاه که هویت پاسداری است، این به معنای یک مفهوم محافظه‌کارانه تلقی نشود.'
    //         },
    //         {
    //             title: 'داستان ۲',
    //             imageUrl: '/images/samples/Medical/m2.jpg',
    //             href: '/images/b1',
    //             author: {
    //                 name: 'امین فخاری',
    //                 url: '#'
    //             },
    //             description: 'این ابتکار جالب که روزی به نام پاسدار را ارتباط بدهند با میلاد مسعود سیدالشهداء (سلام الله علیه)، یک ابتکار پرمغز و جهت‌دهی است. زیرا داعیه‌ی سپاه پاسداران، داعیه‌ی بزرگی است: پاسداری از انقلاب اسلامی، این هویت سپاه که هویت پاسداری است، این به معنای یک مفهوم محافظه‌کارانه تلقی نشود.'
    //         },
    //         {
    //             title: 'داستان ۳',
    //             imageUrl: '/images/samples/Medical/m3.jpg',
    //             href: '/images/b1',
    //             author: {
    //                 name: 'امین فخاری',
    //                 url: '#'
    //             },
    //             description: 'این ابتکار جالب که روزی به نام پاسدار را ارتباط بدهند با میلاد مسعود سیدالشهداء (سلام الله علیه)، یک ابتکار پرمغز و جهت‌دهی است. زیرا داعیه‌ی سپاه پاسداران، داعیه‌ی بزرگی است: پاسداری از انقلاب اسلامی، این هویت سپاه که هویت پاسداری است، این به معنای یک مفهوم محافظه‌کارانه تلقی نشود.'
    //         },
    //         {
    //             title: 'داستان ۴',
    //             imageUrl: '/images/samples/Medical/m3.jpg',
    //             href: '/images/b1',
    //             author: {
    //                 name: 'امین فخاری',
    //                 url: '#'
    //             },
    //             description: 'این ابتکار جالب که روزی به نام پاسدار را ارتباط بدهند با میلاد مسعود سیدالشهداء (سلام الله علیه)، یک ابتکار پرمغز و جهت‌دهی است. زیرا داعیه‌ی سپاه پاسداران، داعیه‌ی بزرگی است: پاسداری از انقلاب اسلامی، این هویت سپاه که هویت پاسداری است، این به معنای یک مفهوم محافظه‌کارانه تلقی نشود.'
    //         }
    //     ];
    //     return stories;
    // }
    constructor(props){
        super(props);
        this.state = {
            stories: []
        }
    }
    componentWillReceiveProps(newProps){
        this.setState({
            stories: newProps.stories == null ? null : newProps.stories
        })
    }

    renderStories(){
        return this.state.stories.map((story, index) => {
            if(index % 3 == 0 & index !=0){
                return (
                    <div key={index}>
                        <PhotoRow4 />
                        <StorySearchCard storyObject={story} />
                    </div>
                );
            }
            return (
                <StorySearchCard key={index} storyObject={story} />
            )
        });
    }

    render(){
        const T = i18n.createComponent();
        return (
            <div>
                {this.renderStories()}
            </div>
        )
    }
}
export default createContainer(() => {
    Meteor.subscribe('Stories');
    return {
        stories: Stories.find().fetch()
    };
}, StoriesSearch);