import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';
import PhotoContainerRowOne from '../../components/rows/PhotoContainerRowOne.jsx';
import PhotoContainerRowFour from '../../components/rows/PhotoContainerRowFour.jsx';
import PhotoContainerRowFive from '../../components/rows/PhotoContainerRowFive.jsx';
import Photos from '/imports/collections/Photos';
import { createContainer } from 'meteor/react-meteor-data';

class ImageSearch extends Component{
    constructor(props){
        super(props);
        this.state = {
            rows: []
        }
    }


    componentWillReceiveProps(newProps) {
        this.setState({
            rows: this.getHomeItems(newProps.photos)
        });
    }

    getHomeItems(photos){

        return [
            // {id: 0, type: '1-large', photoIds: [0]},
            // {id: 1, type: '4-small', photoIds: [1, 2, 3, 4]},
            // {id: 2, type: '5-medium', photoIds: [5, 6, 7, 8, 9]},
            {id: 0, type: '1-large', photos: [photos[0]]},
            {id: 1, type: '4-small', photos: [photos[1], photos[2], photos[3], photos[4]]},
            {id: 2, type: '5-medium', photos: [photos[5], photos[6], photos[7], photos[8], photos[9]]}
        ];
    }

    getRowByType(id, type, photos){
        if(type == '1-large')
            return <PhotoContainerRowOne key={id} photos={photos} />

        if(type == '4-small')
            return <PhotoContainerRowFour key={id} photos={photos} />

        if(type == '5-medium')
            return <PhotoContainerRowFive key={id} photos={photos} />
        // return <div>{id}</div>
    }

    renderRows(rows){
        return rows.map(row => {
            return this.getRowByType(row.id ,row.type, row.photos);
        });
    }

    render(){
        return (
            <div className="archi-container">
                {this.renderRows(this.state.rows)}
            </div>
        );
    }

}
export default createContainer(() => {
    Meteor.subscribe('photos');
    return {
        photos: Photos.find().fetch()
    };
}, ImageSearch);