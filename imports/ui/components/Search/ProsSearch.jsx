import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';
import ProfileSearchCard from '../profile/ProfileSearchCard.jsx';
import PhotoRow4 from '../common/PhotoRow4.jsx';
import ProsSearchCardRow2 from '../rows/ProSearchCardRow2.jsx';
import ProsSearchCardRow1 from '../rows/ProSearchCardRow1.jsx';
import InputGroup from '../common/InputGroup.jsx';
import Profiles from '../../../collections/Profiles';
import {createContainer} from 'meteor/react-meteor-data';

class ProsSearch extends Component{
    // componentWillMount(){
    //     this.pros = [
    //         1, 2, 3, 4, 5, 6, 7
    //     ];
    // }
    constructor(props){
        super(props);
        this.state = {
            pros: null
        }
    }
    componentWillMount(){
        this.setState({
            pros: this.props.pros
        })
    }
    componentWillReceiveProps(newProps){
        this.setState({
            pros: newProps.pros
        })

    }
    renderProCards(){
        let renderObject = [];
        let l = this.state.pros.length;
        for(let i=0; i<this.state.pros.length; i+=2) {
            let users = [];
            // if(i % 4 == 0  && i!=0){
            //     renderObject.push(<PhotoRow4 />);
            // }
            if(i == l - 1) {
                users.push(this.state.pros[i]);
                renderObject.push(<ProsSearchCardRow1 users={users} />);

            }else{
                users.push(this.state.pros[i]);
                users.push(this.state.pros[i+1]);
                renderObject.push(<ProsSearchCardRow2 users={users}/>);
            }
        }
        return renderObject;
    }

    render(){
        const T = i18n.createComponent();
        return (
            <div>
                <div className="archi-row archi-pros-search-toolbar">
                    <div className="col-6">
                        <InputGroup label={i18n.__('common.profession')}></InputGroup>
                    </div>
                    <div className="col-4">
                        <InputGroup label={i18n.__('common.cityLocation')}></InputGroup>
                    </div>
                    <div className="col-2">
                        <button className="btn btn-blue"><T>common.search</T></button>
                    </div>
                </div>
                <div className="archi-row">
                    {this.renderProCards()}
                </div>
            </div>
        )
    }
}
export default createContainer(() => {
    Meteor.subscribe('Profiles');
    return{
        pros: Profiles.find().fetch()
    };
}, ProsSearch)