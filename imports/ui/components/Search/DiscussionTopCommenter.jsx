import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';
import TopCommenter from './TopCommenter.jsx';

export default class DiscussionTopCommenter extends Component{
    
    componentWillMount() {
        this.state = {
            topCommenters: [
                {name: "آبراهام هوسپیان", imgUrl: '/images/assets/fullFakhari.jpg', icon: 'minus'},
                {name: "آبراهام هوسپیان", imgUrl: '/images/assets/fullFakhari.jpg', icon: 'chevron-down'},
                {name: "آبراهام هوسپیان", imgUrl: '/images/assets/fullFakhari.jpg', icon: 'chevron-up'},
                {name: "آبراهام هوسپیان", imgUrl: '/images/assets/fullFakhari.jpg', icon: 'minus'},
                {name: "آبراهام هوسپیان", imgUrl: '/images/assets/fullFakhari.jpg', icon: 'minus'}
            ],
            skip: 0,
            limit: 3
        }
    }

    renderCommenter(){
        return(
            <ul className="vert-list">
                {
                    this.state.topCommenters.slice(this.state.skip, this.state.skip + this.state.limit).map((commenter, index) =>(
                        <li key={index}><TopCommenter commenter={commenter} /></li>
                    ))
                }
            </ul>
        );
    }
    nextPage() {
        this.setState({...this.state, skip: this.state.skip + this.state.limit});
    }
    previousPage(){
            this.setState({...this.state, skip: this.state.skip - this.state.limit});
    }
    
    render(){
        let {skip, limit} = this.state;
        let topCommenterCount = this.state.topCommenters.length;
        const T = i18n.createComponent();
        return(
            <div className="archi-discussion-top-commenter">
                <ul className="horz-list">
                    <li><T>common.today</T></li>
                    <li className="active"><T>common.thisWeek</T></li>
                    <li><T>common.thisMonth</T></li>
                </ul>
                {this.renderCommenter()}
                <div className="navigation">
                    <button className="btn" onClick={this.previousPage.bind(this)} disabled={skip < limit ? true : false}><T>common.previous</T></button>
                    <button className="btn" onClick={this.nextPage.bind(this)} disabled={skip + limit > topCommenterCount ? true : false}><T>common.next</T></button>
                </div>
            </div>
        );
    }
}