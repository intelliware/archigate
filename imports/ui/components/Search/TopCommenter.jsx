import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';


export default class TopCommenter extends Component{

    render(){
        const T = i18n.createComponent();
        let commenter = this.props.commenter;
        return(
            <div className="archi-top-commenter">
                <img src={commenter.imgUrl} alt={commenter.name}/>
                <span>{commenter.name}</span>
                <i className={'fa fa-'+commenter.icon} />
            </div>
        );
    }
}
TopCommenter.propTypes = {
    commenter: React.PropTypes.object.isRequired
}