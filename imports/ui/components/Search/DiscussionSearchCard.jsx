import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';
import Profiles from '../../../collections/Profiles';
import {createContainer} from 'meteor/react-meteor-data';


class DiscussionSearchCard extends Component{


    render(){
        let profile = this.props.profile[0];
        const T = i18n.createComponent();
        let discussion = this.props.discussion;
        if(profile != null)
            return(
                <div className="archi-discussion-search-card-container">
                    <div className="content">
                        <div>
                            <p>{discussion.title}</p>
                            <div>
                                <span>{profile.firstName + ' ' + profile.lastName}</span> / <span>{discussion.createdAt}</span>
                                <div>دیدگاه {/*question.commentsCount*/}</div>
                            </div>

                        </div>
                        <div>
                            <div className="commenter">
                                <div>
                                    {/*<img src={question.lastComment.imgUrl} alt=""/>*/}
                                </div>
                                <div>
                                    <div>{/*question.lastComment.name*/}</div>
                                    <div>{/*question.lastComment.createdAt*/}</div>
                                </div>
                            </div>
                            <div className="comment">
                                <p>{/*question.lastComment.comment*/}</p>
                            </div>
                        </div>
                    </div>
                    <div className="photo">
                        <img src={discussion.imgUrl} alt=""/>
                    </div>
                </div>
            );
        else return null;
    }
}
export default createContainer((params) => {
    Meteor.subscribe('Profiles');
    return {
        profile: Profiles.find({userId: params.discussion.ownerId}).fetch()
    }
}, DiscussionSearchCard)