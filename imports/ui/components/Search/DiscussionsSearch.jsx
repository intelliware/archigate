import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';
import DiscussionEditor from './DiscussionEditor.jsx';
import DiscussionTopCommenter from './DiscussionTopCommenter.jsx';
import DiscussionSearchCard from './DiscussionSearchCard.jsx';
import Discussions from '../../../collections/Discussions';
import {createContainer} from 'meteor/react-meteor-data';

class DiscussionsSearch extends Component{
    constructor(props){
        super(props);
        this.state = {
            discussions: []
        }
    }
    componentWillReceiveProps(newProps){
        this.setState({
            discussions: newProps.discussions == null ? null : newProps.discussions
        })
    }
    render(){
        let discussions = this.state.discussions;
        const T = i18n.createComponent();
        return (
            <div className="archi-discussions-container">
                <DiscussionEditor />
                <DiscussionTopCommenter />
                {
                    discussions.map((discussion, index) =>(
                        <DiscussionSearchCard discussion={discussion} key={index}/>
                    ))
                }
            </div>
        );
    }
}
export default createContainer(() => {
    Meteor.subscribe('Discussions');
    return {
        discussions: Discussions.find().fetch()
    };
}, DiscussionsSearch)