import React, {Component, PropTypes} from 'react';
import PhotoContainer from '../common/PhotoContainer.jsx';

class PhotoContainerRowFour extends Component {
    render() {
        return (
            <div className="archi-row">
                <div className="col-3">
                    <PhotoContainer photo={this.props.photos[0]} componentSize="small" />
                </div>
                <div className="col-3">
                    <PhotoContainer photo={this.props.photos[1]} componentSize="small" />
                </div>
                <div className="col-3">
                    <PhotoContainer photo={this.props.photos[2]} componentSize="small" />
                </div>
                <div className="col-3">
                    <PhotoContainer photo={this.props.photos[3]} componentSize="small" />
                </div>
            </div>
        );
    }
}

PhotoContainerRowFour.propTypes = {
    photos: React.PropTypes.array.isRequired
};

export default PhotoContainerRowFour;