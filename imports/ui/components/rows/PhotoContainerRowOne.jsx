import React, {Component, PropTypes} from 'react';
import PhotoContainer from '../common/PhotoContainer.jsx';

class PhotoContainerRowOne extends Component {
    render() {
        console.log(this.props.photos[0])
        let photo = this.props.photos[0]; 
        return (
            <div className="archi-row">
                <div className="col-12">
                    <PhotoContainer photo={photo} componentSize="large" />
                </div>
            </div>
        );
    }
}

PhotoContainerRowOne.propTypes = {
    photos: React.PropTypes.array.isRequired
};

export default PhotoContainerRowOne;