import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';
import ProfileSearchCard from '../profile/ProfileSearchCard.jsx';

export default class ProsSearchCardRow1 extends Component{

    render(){
        const T = i18n.createComponent();
        return (
            <div className="archi-row archi-profile-search-card-row">
                <div className="col-6 archi-no-padding-left">
                    <ProfileSearchCard user = {this.props.users[0]} />
                </div>
            </div>
        )
    }
}