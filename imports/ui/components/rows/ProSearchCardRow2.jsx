import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';
import ProfileSearchCard from '../profile/ProfileSearchCard.jsx';

export default class ProsSearchCardRow2 extends Component{

    render(){
        const T = i18n.createComponent();
        return (
            <div className="archi-row archi-profile-search-card-row">
                <div className="col-6">
                    <ProfileSearchCard user = {this.props.users[0]} />
                </div>
                <div className="col-6">
                    <ProfileSearchCard user = {this.props.users[1]} />
                </div>
            </div>
        )
    }
}