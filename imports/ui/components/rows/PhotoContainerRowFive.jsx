import React, {Component, PropTypes} from 'react';
import PhotoContainer from '../common/PhotoContainer.jsx';

class PhotoContainerRowFive extends Component {
    render() {
        return (
            <div className="archi-row">
                <div className="col-6">
                    <div className="archi-row">
                        <div className="col-6">
                            <PhotoContainer photo={this.props.photos[0]} componentSize="small" />
                        </div>
                        <div className="col-6">
                            <PhotoContainer photo={this.props.photos[1]} componentSize="small" />
                        </div>
                    </div>
                    <div className="archi-row">
                        <div className="col-6">
                            <PhotoContainer photo={this.props.photos[2]} componentSize="small" />
                        </div>
                        <div className="col-6">
                            <PhotoContainer photo={this.props.photos[3]} componentSize="small" />
                        </div>
                    </div>
                </div>
                <div className="col-6">
                    <PhotoContainer photo={this.props.photos[4]} componentSize="large" />
                </div>
            </div>
        );
    }
}

PhotoContainerRowFive.propTypes = {
    photos: React.PropTypes.array.isRequired
};

export default PhotoContainerRowFive;