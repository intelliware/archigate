import React, {Component} from 'react';
import CommentsContainer from './CommentsContainer.jsx';
import i18n from 'meteor/universe:i18n';

export default class Comment extends Component{

    renderComments(){
        const T = i18n.createComponent();
        return this.props.comments.objects.map((commentWrapper, index) => {
            console.log(commentWrapper);
            console.log('pending: '+this.props.comments.pending);
            if('localId' in commentWrapper){
                console.log('inja');
                for(let i=this.props.comments.pending.length-1; i>=0; i--){
                    if(this.props.comments.pending[i] == commentWrapper.localId)
                        return(
                            <div key={index} className="archi-comment-container archi-row">
                                <div className="col-1">
                                    <div className="archi-vertical-middle"><T>common.reply</T></div>
                                </div>
                                <div className="col-11">
                                    <h4>pending</h4>
                                    <p>{commentWrapper.text}</p>
                                    <div>{commentWrapper.userId}</div>
                                </div>
                            </div>
                        )
                }
            }
            return(
                <div key={index} className="archi-comment-container archi-row">
                    <div className="col-1">
                        <div className="archi-vertical-middle"><T>common.reply</T></div>
                    </div>
                    <div className="col-11">
                        <h4></h4>
                        <p>{commentWrapper.text}</p>
                        <div>{commentWrapper.userId}</div>
                    </div>
                </div>
            );
        })

    }

    render() {
        return(
            <div>
                {this.renderComments()}
            </div>
        );
    }
}
