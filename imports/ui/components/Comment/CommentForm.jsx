import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';


export default class CommentForm extends Component{

    getInitialState(){
        return {commentText: ''};
    }

    sendComment(event){
        event.preventDefault();
        let comment= {
            userId: Meteor.userId(),
            text: this.state.commentText,
            replies: []
        };
        this.props.submitComment(comment);
    }

    changeComment(event){
        this.setState({commentText: event.target.value});
    }

    render(){
        const T = i18n.createComponent();
        return(
            <form onSubmit={this.sendComment.bind(this)}>
                <div className="archi-row archi-comment-form-container">
                    <div className="col-3 archi-no-padding">
                        <div><T>common.replyToComments</T></div>
                        <button type="submit" className="btn btn-white archi-uppercase"><T>common.send</T></button>
                    </div>
                    <div className="col-9 archi-no-padding">
                        <textarea className="form-control" rows="5" onChange={this.changeComment.bind(this)} />
                    </div>
                </div>
            </form>
        );
    }
}