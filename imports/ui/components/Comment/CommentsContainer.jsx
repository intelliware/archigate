import React, {Component} from 'react';
import CommentForm from './CommentForm.jsx';
import Comments from './Comments.jsx';
import CommentsCollection from '../../../collections/Comments';
import {promiseCall, promiseInsert, promiseFind} from '../../../../lib/utils';
import { createContainer } from 'meteor/react-meteor-data';

class CommentsContainer extends Component{

    constructor(props){
        super(props);
        this.comments = [];
        this.state = {
            sege: 'soote',
            comments: []
        };
    }

    componentWillMount(){
        //this.fetchComments();
    }

    submitComment(comment){
        this.setState();
        comment.referenceId = this.props.referenceId;
        //comment
        let localId = this.props.comments.objects.length;
        this.props.comments.pending.push(localId);
        let tmpComment = comment;
        tmpComment.localId = localId;
        this.props.comments.objects.push(tmpComment);
        console.log('comment: '+ JSON.stringify(comment));
        promiseInsert(CommentsCollection, comment, {localId: localId})
            .then((response) => {
                for(let i=this.props.comments.pending.length-1; i>=0; i--){
                    if(this.props.comments.pending[i] == response.tags.localId)
                        this.props.comments.pending.splice(i, 1);
                }
            })
            .catch((e) => {
                console.log(e);
            })
    }

    sege(){
        alert(this.props.comments);
    }

    render(){
        return(
            <div>
                <CommentForm submitComment={this.submitComment.bind(this)} />
                <Comments comments={this.props.comments} />
            </div>
        );
    }
}

CommentsContainer.PropTypes = {
    reference: React.PropTypes.string.isRequired
};

export default CommentsContainer = createContainer((params) => {
    Meteor.subscribe('Comments');
    return{
        comments : {
            objects: CommentsCollection.find({referenceId: params.referenceId}).fetch(),
            pending: []
        }
    }


}, CommentsContainer);