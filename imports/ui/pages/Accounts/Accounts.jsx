import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';
import {Link, IndexLink} from 'react-router';

export default class Accounts extends Component{
    render(){
        const T = i18n.createComponent();
        return (
            <div className="container archi-container">
                <div className="archi-row archi-display-table">
                    <div className="col-6 archi-header-body">
                        <h2><T>accounts.accountsMessageHeader</T></h2>
                        <p><T>accounts.accountsMessageBody</T></p>
                    </div>
                    <div className="col-6 archi-direction-rtl">
                        <img src="/images/assets/2.png" />
                    </div>
                </div>
                <div className="archi-row">
                    <div>
                        <div className="archi-row archi-login-container">
                            <div className="col-9">
                                <div className="archi-row archi-accounts-nav">
                                    <div className="col-4">
                                        <Link to="/accounts/signup" activeClassName="selected"><T>accounts.signUp</T></Link>
                                    </div>
                                    <div className="col-4 hidden-xs"></div>
                                    <div className="col-4 col-xs-6">
                                        <IndexLink to="/accounts" activeClassName="selected"><T>accounts.login</T></IndexLink>
                                    </div>
                                </div>
                                {this.props.children}
                            </div>
                            <div className="col-3">
                                <div className="archi-row archi-accounts-nav">
                                    <div className="col-12">
                                        <span><T>accounts.guide</T></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}