import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';
import ProfileHead from '../../components/profile/ProfileHead.jsx';
import ProfileHeadEdit from '../../components/profile/ProfileHeadEdit.jsx';
import ProfilePageNavbar from '../../components/profile/ProfilePageNavbar.jsx';
import ProfilePageNavbarEdit from '../../components/profile/ProfilePageNavbarEdit.jsx';
import ProfilePageSidebarEdit from '../../components/profile/ProfilePageSidebarEdit.jsx';
import ProfilePageSidebar from '../../components/profile/ProfilePageSidebar.jsx';
import Profiles from '../../../collections/Profiles';
import { createContainer } from 'meteor/react-meteor-data';
import {promiseCall, promiseInsert, promiseFind} from '../../../../lib/utils';


class ProfilePage extends Component {
    componentWillMount(){
        this.state = {
            profile: this.props.profile[0],
            handle: this.props.handle
        }
    }


    componentWillReceiveProps(newProps){
        this.setState({
            profile: newProps.profile[0],
            handle: newProps.handle
        })
    }

    renderHead(profile){
        if(Meteor.userId()){
            // console.log(Meteor.userId());
            // console.log(profile.id);

            if(Meteor.userId() != profile.userId)
                return <ProfileHead profile={profile} />
            else
                return <ProfileHeadEdit profile={profile} />
        }
    }

    renderSidebar(profile) {
        // console.log(profile.id);
        // console.log(Meteor.userId());
        if (Meteor.userId() != profile.userId)
            return (
                <ProfilePageSidebar profile={profile}/>
            );
        else
            return (
                <ProfilePageSidebarEdit profile={profile} />
            );
    }
    renderNavbar(profile){
        if(Meteor.userId() != profile.userId)
            return(
                <ProfilePageNavbar profile={profile} />
            );
        else
            return(
                <ProfilePageNavbarEdit profile={profile} />
            );
    }

    render() {
        if(!this.state.handle.ready())
            return null;

        let profile = this.state.profile;
        console.log(profile);
        return (
            <div className="archi-container">
                {this.renderHead(profile)}
                <div className="archi-row">
                    <div className="col-3">
                        {this.renderSidebar(profile)}
                    </div>
                    <div className="col-9 archi-no-padding">
                        {this.renderNavbar(profile)}
                         {this.props.children}
                    </div>
                </div>
            </div>
        );
    }
}

export default createContainer((params) => {
    const handle = Meteor.subscribe('Profiles');
    return {
        profile: Profiles.find({userId: params.params.id}).fetch(),
        handle: handle
    };
}, ProfilePage);
