import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';
import StarRating from '../../components/common/StarRating.jsx';
import RelatedPhotos from '../../components/common/RelatedPhotos.jsx';
import CommentsContainer from '../../components/Comment/CommentsContainer.jsx';
import ProfileCard from '../../components/profile/ProfileCard.jsx';
import Photos from '../../../collections/Photos';
import {createContainer} from 'meteor/react-meteor-data';


class Photo extends Component{
    componentWillMount(){
        this.state = {
            photo: this.props.photo[0] == undefined ? {} : this.props.photo[0]
        }
        this.image = {
            url: '/images/samples/Kitchen/k1.jpg',
            href: '/images/samples/Bedroom/be5.jpg',
            caption: 'طراحی داخلی واحد اداری ایرانسل',
            category: 'واحد اداری'
        };
    }
    componentWillReceiveProps(newProps){
        this.setState({
            photo: newProps.photo.length == 0 ? {} : newProps.photo[0]
        })
    }
    render(){
        const T = i18n.createComponent();
        let photo = this.state.photo;
        return(
            <div className="archi-row archi-photo-page-container">
                <div className="col-4">
                    <ProfileCard />
                    <RelatedPhotos scope="project" image={this.image} />
                    <RelatedPhotos scope="category" image={this.image} />
                    <RelatedPhotos scope="category" image={this.image} />
                    <RelatedPhotos scope="category" image={this.image} />
                    <CommentsContainer referenceId={this.props.params.id}/>

                </div>
                <div className="col-8">
                    <img src={photo.imgUrl} />
                    <a className="archi-vertical-middle left" href="#"><img src="/images/assets/6.png" /></a>
                    <a className="archi-vertical-middle right" href="#"><img src="/images/assets/5.png" /></a>
                    <div className="archi-horizental-center bottom">
                        <ul className="horz-list">
                            <li><button className="btn btn-white archi-uppercase"><T>common.save</T></button></li>
                            <li><button className="btn btn-white archi-uppercase"><T>common.share</T></button></li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}
export default createContainer((params) => {
    Meteor.subscribe('photos');
    return{
        photo: Photos.find({_id: params.params.id}).fetch()
    }
}, Photo)