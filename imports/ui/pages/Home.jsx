import React, {Component, PropTypes} from 'react';
import i18n from 'meteor/universe:i18n';
import PhotoContainerRowOne from '../components/rows/PhotoContainerRowOne.jsx';
import PhotoContainerRowFour from '../components/rows/PhotoContainerRowFour.jsx';
import PhotoContainerRowFive from '../components/rows/PhotoContainerRowFive.jsx';
import {promiseInsert, promiseUpdate, promiseFind} from '/lib/utils';
import Photos from '/imports/collections/Photos';
import { createContainer } from 'meteor/react-meteor-data';

class Home extends Component{
    componentWillMount(){
        this.state = {
            rows: this.getHomeItems(this.props.photos)
        }
    }
    componentWillReceiveProps(newProps){
        this.setState({
            rows: this.getHomeItems(newProps.photos)
        });
        // let p1 = promiseFind(Profiles, {_id: "LQ3aLJuk6dNWsgAM7"}).then((doc) => {
        //     console.log(doc);
        // });
        // const profiles = Meteor.subscribe('Profile', this.props.profiles);

        // console.log(Categories.find().fetch());
        // console.log(this.props.categories);
    }

    getHomeItems(photos){
        
        return [
            // {id: 0, type: '1-large', photoIds: [0]},
            // {id: 1, type: '4-small', photoIds: [1, 2, 3, 4]},
            // {id: 2, type: '5-medium', photoIds: [5, 6, 7, 8, 9]},
            {id: 0, type: '1-large', photos: [photos[0]]},
            {id: 1, type: '4-small', photos: [photos[1], photos[2], photos[3], photos[4]]},
            {id: 2, type: '5-medium', photos: [photos[5], photos[6], photos[7], photos[8], photos[9]]}
        ];
    }

    getRowByType(id, type, photos){
        if(type == '1-large')
            return <PhotoContainerRowOne key={id} photos={photos} />
        
        if(type == '4-small')
            return <PhotoContainerRowFour key={id} photos={photos} />

        if(type == '5-medium')
            return <PhotoContainerRowFive key={id} photos={photos} />
        // return <div>{id}</div>
    }

    renderRows(rows){
        return rows.map(row => {
            return this.getRowByType(row.id ,row.type, row.photos);
        });
    }

    render(){
        return (
            <div className="container archi-container">
                {this.renderRows(this.state.rows)}
            </div>
        );
    }
}
// Home.propTypes = {
//     cats: PropTypes.array.isRequired
// };
export default createContainer(() => {
    Meteor.subscribe('photos');
    return {
        photos: Photos.find().fetch()
    };
}, Home);

// <div className="archi">
//                     <div className="col-sm-12 archi-no-padding">
//                         <PhotoContainer photoId="0" componentSize="large"/>
//                     </div>
//                 </div>
//                 <div className="row">
//                     <div className="col-xs-12 col-md-3 archi-no-padding">
//                         <PhotoContainer photoId="1" componentSize="small"/>
//                     </div>
//                     <div className="col-xs-12 col-md-3 archi-no-padding">
//                         <PhotoContainer photoId="2" componentSize="small"/>
//                     </div>
//                     <div className="col-xs-12 col-md-3 archi-no-padding">
//                         <PhotoContainer photoId="3" componentSize="small"/>
//                     </div>
//                     <div className="col-xs-12 col-md-3 archi-no-padding">
//                         <PhotoContainer photoId="4" componentSize="small"/>
//                     </div>
//                 </div>
//                 <div className="row">
//                     <div className="col-xs-12 col-md-6 archi-no-padding">
//                         <div className="col-xs-12 col-md-6 archi-no-padding">
//                             <PhotoContainer photoId="5" componentSize="small"/>
//                         </div>
//                         <div className="col-xs-12 col-md-6 archi-no-padding">
//                             <PhotoContainer photoId="6" componentSize="small"/>
//                         </div>
//                         <div className="col-xs-12 col-md-6 archi-no-padding">
//                             <PhotoContainer photoId="7" componentSize="small"/>
//                         </div>
//                         <div className="col-xs-12 col-md-6 archi-no-padding">
//                             <PhotoContainer photoId="8" componentSize="small"/>
//                         </div>
//                     </div>
//                     <div className="col-xs-12 col-md-6 archi-no-padding">
//                         <PhotoContainer photoId="9" componentSize="large"/>
//                     </div>
//                 </div>