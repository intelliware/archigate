import React, {Component} from 'react';
import Home from './Home.jsx';
import NavbarNew from './../components/navbar/NavbarNew.jsx';
import i18n from 'meteor/universe:i18n';

export default class App extends Component{
    constructor(props){
        super(props);
        i18n.setLocale('fa-IR');
        this.state = {isRTL: i18n.isRTL()}
    }
    
    _localeChanged(locale){
        this.setState({isRTL: i18n.isRTL(locale)});
    }

    componentWillMount(){
        i18n.onChangeLocale(this._localeChanged.bind(this));
    }

    componentWillUnmount(){
        i18n.offChangeLocale(this._localeChanged.bind(this));
    }

    render(){
        return (
            <div className={ this.state.isRTL ? 'rtl' : 'ltr' }>
                <NavbarNew />
                {this.props.children}
            </div>
        );
    }

}