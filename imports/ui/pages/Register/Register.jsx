import React, {Component} from 'react';

class Register extends Component{
    constructor(props){
        super(props);
        this.state = {
            profile: {name: 'sege'}
        }
    }

    render(){
        return (
            <div className="col-12 archi-register-container">
                {this.props.children}
            </div>
        );
    }
}
export default Register