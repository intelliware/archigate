import React, {Component} from 'react';
import i18n from 'meteor/universe:i18n';
import {Link, IndexLink} from 'react-router';

export default class Search extends Component{
    render(){
        let headerConf = {
            '/search': {
                header: 'common.exteriorDesign',
                body: 'common.kosSher',
                imageSource: '/images/samples/exterior/imageHeader.jpg'
            },
            '/search/professionals': {
                header: 'common.exteriorDesign',
                body: 'common.kosSher',
                imageSource: '/images/samples/exterior/imageHeader.jpg'
            },
            '/search/discussions': {
                header: 'common.exteriorDesign',
                body: 'common.kosSher',
                imageSource: '/images/samples/exterior/imageHeader.jpg'
            },
            '/search/stories': {
                header: 'common.exteriorDesign',
                body: 'common.kosSher',
                imageSource: '/images/samples/exterior/imageHeader.jpg'
            }
        };
        const T = i18n.createComponent();
        return (
            <div className="container archi-container">
                <div className="archi-row">
                    <div className="col-6 archi-header-body">
                        <div>
                            <h2><T>{headerConf[this.props.location.pathname].header}</T></h2>
                            <p><T>{headerConf[this.props.location.pathname].body}</T></p>
                        </div>
                    </div>
                    <div className="col-6 archi-direction-rtl archi-search-header-image">
                        <img src={headerConf[this.props.location.pathname].imageSource} />
                    </div>
                </div>
                <div className="archi-row">
                    <div>
                        <div className="archi-row archi-search-container">
                            <div className="col-3">
                                <div className="archi-row archi-accounts-nav">
                                    <div className="col-12">
                                        <span><T>accounts.guide</T></span>
                                    </div>
                                </div>
                            </div>
                            <div className="col-9">
                                <div className="archi-row archi-search-nav">
                                    <div className="col-3">
                                        <IndexLink to="/search" activeClassName="selected"><T>common.photos</T></IndexLink>
                                    </div>
                                    <div className="col-3">
                                        <Link to="/search/professionals" activeClassName="selected"><T>common.pros</T></Link>
                                    </div>
                                    <div className="col-3">
                                        <Link to="/search/discussions" activeClassName="selected"><T>common.discussions</T></Link>
                                    </div>
                                    <div className="col-3">
                                        <Link to="/search/stories" activeClassName="selected"><T>common.stories</T></Link>
                                    </div>
                                </div>
                                {this.props.children}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}