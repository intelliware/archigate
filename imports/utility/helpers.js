export function requireAuth(nextState, replace){
    if (!Meteor.userId())
        replace({ pathname: '/accounts', state: { nextPathname: nextState.location.pathname } })
}