/**
 * Created by mehdi on 7/6/16.
 */
import Comments from '../imports/collections/Comments';
if(Meteor.isClient)
    Meteor.subscribe('Comments');
export const promiseInsert = (collection, document, tags) => {
    return new Promise((resolve, reject) => {
        Meteor.setTimeout(() => {collection.insert(document, (error, id) => {
            if (error) reject(error);
            else resolve({id, tags});
        })}, 5000);
    });
};

export const promiseUpdate = (collection, selector, document, options={}) => {
    return new Promise((resolve, reject) => {
        collection.update(selector, document, options, (error, no_affected) => {
            if (error)
                reject(error);
            else
                resolve(no_affected);
        })
    });
};

export const promiseFind = (collection, query) => {
    return new Promise((resolve, reject) => {
        resolve(collection.find(query).fetch());
    });
};


export const promiseCall = (methodName, args) => {
    return new Promise((resolve, reject) => {
        Meteor.setTimeout(() => {
            Meteor.apply(methodName, args, (error, result) => {
                if (error)
                    reject(error);
                else {
                    resolve(result);
                }
            });
        }, 5000);
    });
};
